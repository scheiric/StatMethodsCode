""" Example of event clasification using tensorflow/keras

    Make sure to run the ML_example01.py or ML_example01.bonus.py beforhand

    To run this example, you will need the following packages, which are best
    installed in the python virtual environment

    python3 -m venv venv
    . venv/bin/activate
    pip install awkward
    pip install pandas    
    pip install awkward-pandas
    pip install uproot
    pip install tensorflow
    pip install matplotlib

    To execute the code:
    . venv/bin/activate
    python3 -i python/MVA_example02.py

"""

import tensorflow as tf
import uproot
import awkward as ak
import numpy as np
import pandas as pd
import os
import matplotlib.pyplot as plt

## Folders with trained models
model_names = [ 'model_trained_mlp',  # MLP: run ML_example01.py to train this model
                'model_trained_hw',   # Simplified highway network: run ML_example01.bonus.py to train this model
              ]

## set a seed for reproducibility
np.random.seed(42)
tf.random.set_seed(42)

## load the TTree Lam from the input file
tree = uproot.open("data/skimThin.mc10.Lam+LamBar.MinBias.SimpleTree.01.root")["Lam"]

## list of variables to be loaded from the input file
variables = [
  "VTX_chi2[:,:]"      ,  # this variable is of type vector<double>. We take all elements of the vector
  "VTX_lxy[:,:,0]"     ,  # this variable is of type vector<vector<double>>. We take all elements of the first vector, but only the first element of the second vector
  "VTX_lxyError[:,:,0]",  # this variable is of type vector<vector<double>>
  "VTX_a0[:,:,0]"      ,  # this variable is of type vector<vector<double>>
  "VTX_a0Error[:,:,0]" ,  # this variable is of type vector<vector<double>>
  "VTX_px[:,:]"        ,  # this variable is of type vector<double>
  "VTX_py[:,:]"        ,  # this variable is of type vector<double>
  "isSignal[:,:]"      ,  # this variable is of type vector<int>
]

## load the variables into the pandas dataframe
df = tree.arrays(variables, library='ak')
df = ak.to_dataframe(df) # NOTE: in older versions of ak, this is called ak.to_pandas

## cast all variables into the same type so that they can be used in a single tensor
df[variables] = df[variables].astype(np.float32)

## debug printout
print(df)

## convert the dataframes into tensorflow dataset. We have to convert the dataframe into dict so that 
## tensorflow can work with it
dataset = tf.data.Dataset.from_tensor_slices(dict(df))

## Process the dataset. The function does three things:
## 1/ calculates new variables which were not present in the original file
## 3/ stacks the new variables into a single tensor "data"
## 3/ Returns the new tensor "data" and the variable "isSignal" as two separate tensors. 
# # The 'isSignal' tensor will serve as an ground-truth label during training
@tf.function
def prepare_sample(sample):
  ## process the label
  label = sample['isSignal[:,:]']   # label is called "isSignal" in our data
  label = tf.cast(label, tf.int32)  # we want to cast it to int32 type to save memory
  
  ## calculate new variables
  lxySig = sample["VTX_lxy[:,:,0]"] / sample["VTX_lxyError[:,:,0]"]     # decay length significance
  a0Sig  = sample["VTX_a0[:,:,0]"]  / sample["VTX_a0Error[:,:,0]"]      # impact parameter significance
  pt     = tf.sqrt(sample["VTX_px[:,:]"]**2 + sample["VTX_py[:,:]"]**2) # transverse momentum

  ## stack all variables into a single tensor
  data = tf.stack([sample["VTX_chi2[:,:]"], lxySig, a0Sig, pt])

  return data, label

## apply the function to the dataset
dataset = dataset.map(prepare_sample)

## split dataset into training and validation samples. We use half/half mixture as in the TMVA example
print("Number of all events:        ", len(df))
print("Number of validation events: ", len(df)-len(df)//2)
val_dataset   = dataset.skip(len(df)//2)

## split signal and background events into separate samples. We use the "filter" method.
@tf.function
def filter_sig(data, label):
  return label==1

@tf.function
def filter_bkg(data, label):
  return label==0

## Apply the above-defined functions
val_dataset_sig = val_dataset.filter(filter_sig)
val_dataset_bkg = val_dataset.filter(filter_bkg)

## group samples to batches
val_dataset_sig = val_dataset_sig.batch(64)
val_dataset_bkg = val_dataset_bkg.batch(64)

## prefetching intrusts the dataset to utilize more cores for data preparation, so the DNN doesn't wait for the data
## you can specify a concrete number of cores or pass the tf.data.AUTOTUNE, which will do it dynamically
val_dataset_sig = val_dataset_sig.prefetch(tf.data.AUTOTUNE)
val_dataset_bkg = val_dataset_bkg.prefetch(tf.data.AUTOTUNE)

## At this point we can drop the "label" tensor because we no longer need it
@ tf.function
def pick_only_data(data, label):
  return data

val_dataset_sig = val_dataset_sig.map(pick_only_data)
val_dataset_bkg = val_dataset_bkg.map(pick_only_data)

############################
## Model evaluation
############################
figs = []
for i,model_name in enumerate(model_names):
  ## load the trained model
  if os.path.exists(model_name):
    model = tf.keras.models.load_model(model_name)
    print("Evaluating model", model_name)
  else:
    print("Model {} does not exist. Run the training script first.".format(model_name))
    continue

  ## evaluate on the valiadation dataset
  ## We use the method model.predict(data) which is suitable for processing the whole dataset. 
  ## For event-by-event evaluation, you can use the __call__ method: model(event).
  ## However, the second method is slower
  y_val_sig = model.predict(val_dataset_sig)
  y_val_bkg = model.predict(val_dataset_bkg)

  # draw the plots
  plt.figure(i)
  plt.hist(y_val_sig, bins=50, range=(0,1), histtype='step', label='val sig', alpha=0.5)
  plt.hist(y_val_bkg, bins=50, range=(0,1), histtype='step', label='val bkg', alpha=0.5)
  plt.title(model_name)
  plt.ylabel("Events")
  plt.xlabel('Prediction')
  plt.legend(loc='best')

# show the plots
plt.show()





