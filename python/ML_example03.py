""" Example of event clasification using tensorflow/keras

    Make sure to run the ML_example01.py or ML_example01.bonus.py beforhand

    To run this example, you will need the following packages, which are best
    installed in the python virtual environment

    python3 -m venv venv
    . venv/bin/activate
    pip install awkward
    pip install pandas    
    pip install awkward-pandas
    pip install uproot
    pip install tensorflow
    pip install matplotlib

    To execute the code:
    . venv/bin/activate
    python3 -i python/MVA_example02.py

"""

import tensorflow as tf
import uproot
import awkward as ak
import numpy as np
import pandas as pd
import os
import matplotlib.pyplot as plt

## Folders with trained models
model_names = [ 'model_trained_mlp',  # MLP: run ML_example01.py to train this model
                'model_trained_hw',   # Simplified highway network: run ML_example01.bonus.py to train this model
              ]

## Cuts on the NN output chosen by-eye so that both models accept about the same amount of background in the sidebands around
## the Lambda mass peak
model_cuts = [ 0.59,
              0.56,
]

## set a seed for reproducibility
np.random.seed(42)
tf.random.set_seed(42)

## load the TTree Lam from the input file
tree = uproot.open("data/skimThin.mc10.Lam+LamBar.MinBias.SimpleTree.01.root")["Lam"]  ## MC simulation


## list of variables to be loaded from the input file
variables = [
  "VTX_chi2[:,:]"      ,  # this variable is of type vector<double>. We take all elements of the vector
  "VTX_lxy[:,:,0]"     ,  # this variable is of type vector<vector<double>>. We take all elements of the first vector, but only the first element of the second vector
  "VTX_lxyError[:,:,0]",  # this variable is of type vector<vector<double>>
  "VTX_a0[:,:,0]"      ,  # this variable is of type vector<vector<double>>
  "VTX_a0Error[:,:,0]" ,  # this variable is of type vector<vector<double>>
  "VTX_px[:,:]"        ,  # this variable is of type vector<double>
  "VTX_py[:,:]"        ,  # this variable is of type vector<double>
  "VTX_mass[:,:]"      ,  # this variable is of type vector<double>
]

## load the variables into the pandas dataframe
print("Loading data")
df = tree.arrays(variables, library='ak')

print("Converting to dataframe")
df = ak.to_dataframe(df) # NOTE: in older versions of ak, this is called ak.to_pandas

## cast all variables into the same type so that they can be used in a single tensor
df[variables] = df[variables].astype(np.float32)

## debug printout
print(df)

## convert the dataframes into tensorflow dataset. We have to convert the dataframe into dict so that 
## tensorflow can work with it
dataset = tf.data.Dataset.from_tensor_slices(dict(df))

## Process the dataset. The function does three things:
## 1/ calculates new variables which were not present in the original file
## 3/ stacks the new variables into a single tensor "data"
@tf.function
def prepare_sample(sample):
    
  ## calculate new variables
  lxySig = sample["VTX_lxy[:,:,0]"] / sample["VTX_lxyError[:,:,0]"]     # decay length significance
  a0Sig  = sample["VTX_a0[:,:,0]"]  / sample["VTX_a0Error[:,:,0]"]      # impact parameter significance
  pt     = tf.sqrt(sample["VTX_px[:,:]"]**2 + sample["VTX_py[:,:]"]**2) # transverse momentum

  ## stack all variables into a single tensor
  data = tf.stack([sample["VTX_chi2[:,:]"], lxySig, a0Sig, pt])

  return data

## apply the function to the dataset
dataset = dataset.map(prepare_sample)

## group samples to batches. This is not strictly needed for evaluation, but it will be done internally anyway to speed things up.
dataset = dataset.batch(64)

## prefetching intrusts the dataset to utilize more cores for data preparation, so the DNN doesn't wait for the data
## you can specify a concrete number of cores or pass the tf.data.AUTOTUNE, which will do it dynamically
dataset = dataset.prefetch(tf.data.AUTOTUNE)

############################
## Model evaluation
############################
results = []
for i,model_name in enumerate(model_names):
  ## load the trained model
  if os.path.exists(model_name):
    model = tf.keras.models.load_model(model_name)
    print("Evaluating model", model_name)
  else:
    print("Model {} does not exist. Run the training script first.".format(model_name))
    continue

  ## evaluate on the valiadation dataset
  ## We use the method model.predict(data) which is suitable for processing the whole dataset. 
  ## For event-by-event evaluation, you can use the __call__ method: model(event).
  ## However, the second method is slower
  y = model.predict(dataset)

  ## NOTE model.predict returns the result in a form [[y_0], [y_1], ...]
  ## We change the shape from (n, 1) --> (n,)
  y = np.reshape(y, (y.shape[0],))

  ## perform the selection. The result is a boolean array with the same shape as y
  sel = y > model_cuts[i]

  ## apply the selection on the "mass" column of the original dataframe
  mass_sel = df['VTX_mass[:,:]'][sel]
  y_sel = y[sel]

  ## debug printout
  print("First 10 results after selection y > {}".format(model_cuts[i]))
  print("{:4s} {:10s} {:10s}".format('i'.rjust(4), 'y'.rjust(10), 'mass'.rjust(10)))
  for ii,(yy,mass) in enumerate(zip(y_sel, mass_sel)):
    print("{:4d} {:10.3f} {:10.3f}".format(ii, yy, mass))
    if ii>10: break

  ## store for the further use
  results.append(mass_sel)

## loop over models and their results
for model_name,result in zip(model_names, results):
  ## plot the mass histogram
  plt.hist(result, bins=32, range=(1100, 1132), histtype='step', label=model_name, alpha=0.5)

# show the plots
plt.ylabel("Events")
plt.xlabel('m [MeV]')
plt.legend(loc='best')
plt.show()





