""" Example of MLP using Tensorflow/Keras package

    To run this example, you will need the following packages, which are best
    installed in the python virtual environment

    python3 -m venv venv
    . venv/bin/activate
    pip install awkward
    pip install pandas    
    pip install awkward-pandas
    pip install uproot
    pip install tensorflow

    To execute the code:
    . venv/bin/activate
    python3 python/MVA_example01.py

"""

import tensorflow as tf
import uproot
import awkward as ak
import numpy as np
import pandas as pd

## set a seed for reproducibility
np.random.seed(42)
tf.random.set_seed(42)

## load the TTree Lam from the input file
tree = uproot.open("data/skimThin.mc10.Lam+LamBar.MinBias.SimpleTree.01.root")["Lam"]

## list of variables to be loaded from the input file
variables = [
  "VTX_chi2[:,:]"      ,  # this variable is of type vector<double>. We take all elements of the vector
  "VTX_lxy[:,:,0]"     ,  # this variable is of type vector<vector<double>>. We take all elements of the first vector, but only the first element of the second vector
  "VTX_lxyError[:,:,0]",  # this variable is of type vector<vector<double>>
  "VTX_a0[:,:,0]"      ,  # this variable is of type vector<vector<double>>
  "VTX_a0Error[:,:,0]" ,  # this variable is of type vector<vector<double>>
  "VTX_px[:,:]"        ,  # this variable is of type vector<double>
  "VTX_py[:,:]"        ,  # this variable is of type vector<double>
  "isSignal[:,:]"      ,  # this variable is of type vector<int>
]

## load the variables into the pandas dataframe
df = tree.arrays(variables, library='ak')
df = ak.to_dataframe(df) # NOTE: in older versions of ak, this is called ak.to_pandas

## cast all variables into the same type so that they can be used in a single tensor
df[variables] = df[variables].astype(np.float32)

## debug printout
print(df)

## convert the dataframes into tensorflow dataset. We have to convert the dataframe into dict so that 
## tensorflow can work with it
dataset = tf.data.Dataset.from_tensor_slices(dict(df))

## Process the dataset. The function does three things:
## 1/ calculates new variables which were not present in the original file
## 3/ stacks the new variables into a single tensor "data"
## 3/ Returns the new tensor "data" and the variable "isSignal" as two separate tensors. 
# # The 'isSignal' tensor will serve as an ground-truth label during training
@tf.function
def prepare_sample(sample):
  ## process the label
  label = sample['isSignal[:,:]']   # label is called "isSignal" in our data
  label = tf.cast(label, tf.int32)  # we want to cast it to int32 type to save memory
  
  ## calculate new variables
  lxySig = sample["VTX_lxy[:,:,0]"] / sample["VTX_lxyError[:,:,0]"]     # decay length significance
  a0Sig  = sample["VTX_a0[:,:,0]"]  / sample["VTX_a0Error[:,:,0]"]      # impact parameter significance
  pt     = tf.sqrt(sample["VTX_px[:,:]"]**2 + sample["VTX_py[:,:]"]**2) # transverse momentum

  ## stack all variables into a single tensor
  data = tf.stack([sample["VTX_chi2[:,:]"], lxySig, a0Sig, pt])

  return data, label

## apply the function to the dataset
dataset = dataset.map(prepare_sample)

## split dataset into training and validation samples. We use half/half mixture as in the TMVA example
print("Number of all events:      ", len(df))
print("Number of training events: ", len(df)//2)
train_dataset = dataset.take(len(df)//2)
val_dataset   = dataset.skip(len(df)//2)

## shuffle the training dataset
train_dataset = train_dataset.shuffle(buffer_size=1024)

## group samples to batches
train_dataset = train_dataset.batch(64)
val_dataset   = val_dataset.batch(64)

## prefetching intrusts the dataset to utilize more cores for data preparation, so the DNN doesn't wait for the data
## you can specify a concrete number of cores or pass the tf.data.AUTOTUNE, which will do it dynamically
train_dataset = train_dataset.prefetch(tf.data.AUTOTUNE)
val_dataset   = val_dataset.prefetch(tf.data.AUTOTUNE)

#####################
## Build the NN model
#####################

## we need to create the normalization layer before other layers
normalizer = tf.keras.layers.Normalization()

## The following function just pick data tensor from the (data, label) dataset
@ tf.function
def pick_only_data(data, label):
  return data

## Extract the number of variables in the NN input. In this example we know it's 4, but let's show how to do it 
## with he tf.data.Dataset class:
inputShape = train_dataset.map(pick_only_data).element_spec.shape  # this returns the input tensor shape including the batch dimension
nVar = inputShape[-1]  # number of input variables is the last dimension of the input tensor shape

## calculate the mean and variance of the training dataset variables and setup the norm. layer
normalizer.adapt(train_dataset.map(pick_only_data))

# here we start to build the network
# we utilize the so called 'functional API' (https://www.tensorflow.org/guide/keras/functional)
# input layer needs to know the shape of an sample, here we omit the batch dimension
input = tf.keras.layers.Input(shape=(nVar, ))

# firstly we apply the normalization layer on the input
hidden = normalizer(input)

# we create a Dense layer (ie. Liner Perceptron, ie. Fully Connected Layer) with the ReLU = max(0, x) activation function
layer_size = nVar + 5  # we used this hidden layer size in the TMVA example. In practice, larger layers can be used
hidden = tf.keras.layers.Dense(layer_size, activation=tf.nn.relu)(hidden)

## ## OPTIONAL:
## after each dense layer usually it is good to use the dropout layer to avoid overfitting.
## This layer masks (drops out) a node from previous layer with probabilty 'dropout'. This is done for all nodes from previous layer
## Most often it is good to set the DROPOUT to 0.5 and increase the number number of neurons (layer_size).
## In this example, we only have 9 hidden neurons and just one hidden layer, so overfitting is not an issue.
## UNCOMMENT the following two lines to examine the dropout regularization.
# dropout = 0.5
# hidden = tf.keras.layers.Dropout(dropout)(hidden)

# output layer is just one number between 0 and 1, thats why we use the sigmoid=1/(1+exp(-x)) activation to get it
output = tf.keras.layers.Dense(1, activation=tf.nn.sigmoid)(hidden)

# here we create the model with input and output
model = tf.keras.Model(inputs=input, outputs=output)

# we need to compile the model, ie. to specify the loss we want to minimize,
# optimizer (which is used to minimize the loss) and metrics (which are used to evaluate the model)
# in our case we use the binary cross-entropy loss, which is good for binary classification
# we use the Adam optimizer, which is a good default choice
# we use the accuracy metric, which is just the fraction of correct predictions, and AUC, which is the area under the ROC curve
model.compile(optimizer=tf.optimizers.Adam(),
              loss=tf.keras.losses.BinaryCrossentropy(),
              metrics=[tf.keras.metrics.BinaryAccuracy(), tf.keras.metrics.AUC()])

# here we print the model summary, outline
model.summary()

# this is the part where the training happens, we specify the number of epochs we want to train for and validation dataset
# validation will happen after each epoch
epochs = 10
model.fit(train_dataset, epochs=epochs, validation_data=val_dataset)

# Evaluate the model with the training and validation dataset
print("Evaluate on training data")
model.evaluate(val_dataset)

print("Evaluate on validation data")
model.evaluate(val_dataset)

## save the model
print("Saving model to: model_trained_mlp")
model.save("model_trained_mlp")
