
/*
 * example of the constrained likelihood fit.
 * We will fit the mean mass and resolution on a control sample and
 * use it to extract the number of events in the signal sample
 */

#include "RooDataSet.h"
#include "RooRealVar.h"
#include "RooGaussian.h"
#include "RooChebychev.h"
#include "RooPolynomial.h"
#include "RooAddPdf.h"
#include "RooPlot.h"
#include "RooDataSet.h"
#include "TCanvas.h"
#include "TH1D.h"

using namespace RooFit;

// main function
void MLE_example10() {
  
  int nEvt         = 300;  // size of the data sample
  int nEvt_control = 2000; // size of the control sample
  
  RooRealVar m("m", "m", 0, 100, 150, "GeV"); // mass

  //---------------------------------  
  // define model for the comtrol samples.
  //---------------------------------  
  RooRealVar mean_control ("mean", "#mu", 125, 80, 200, "GeV"); // if no range is given, the value is fixed
  RooRealVar sigma_control("sigma", "#sigma", 2., 0, 10);
  RooRealVar fSig_control ("fSig", "f_{sig}", 0.9, 0, 1);
  
  RooGaussian  sigPdf_control("sigPdf_control", "sigPdf_control", m, mean_control, sigma_control);
  RooPolynomial bkgPdf_control("bkgPdf_control", "bkgPdf_control", m);
  RooAddPdf model_control("model_control", "", RooArgSet(sigPdf_control, bkgPdf_control), RooArgSet(fSig_control));
  
  // generate the data
  RooDataSet* data_control = model_control.generate(m, nEvt_control);
  
  // perform fit on control sample to extract mean mass and resolution:
  model_control.fitTo(*data_control);

  // display  
  new TCanvas("c1", "Control sample", 200, 200, 800, 600);
  RooPlot* frame1 = m.frame();
  
  // draw data and PDFs onto the frame1
  data_control ->plotOn(frame1, Binning(30));
  model_control.plotOn(frame1, LineColor(kBlue), LineWidth(3));
  frame1->Draw();
    
  //---------------------------------  
  // define model for the signal sample.
  //---------------------------------  
  // The signal sample will have different signal fraction and background,
  // but it has the same mass and mass resolution:
  
  
  //---------------------------------  
  // define model parameters. Again, class RooRealVar is used.
  //---------------------------------  
  RooRealVar mean ("mean", "#mu", 125, 80, 200, "GeV"); // if no range is given, the value is fixed
  RooRealVar sigma("sigma", "#sigma", 2., 0, 10);
  RooRealVar slope("slope", "a_{1}", -0.8, -1, 1);
  RooRealVar ns   ("ns", "ns", 0.2*nEvt, 0, 1000000);
  RooRealVar nb   ("nb", "nb", (1-0.2)*nEvt, 0, 1000000);
  
  RooGaussian  sigPdf("sigPdf", "sigPdf", m, mean, sigma);
  RooChebychev bkgPdf("bkgPdf", "bkgPdf", m, RooArgSet(slope));
  RooAddPdf model("model", "gsigPdf + bkgPdf", RooArgSet(sigPdf, bkgPdf), RooArgSet(ns,nb));

  // generate the signal sample data
  RooDataSet* data = model.generate(m, nEvt);
  
  // now we create gaussian constraints for mean mass and sigma.
  // We have plugged in the best values and uncertainties from the control sample fit:
  RooRealVar mean_bestValue("mean_bestValue", "mean_bestValue", mean_control.getVal());
  RooRealVar mean_error("mean_error", "mean_error", mean_control.getError());

  RooRealVar sigma_bestValue("sigma_bestValue", "sigma_bestValue", sigma_control.getVal());
  RooRealVar sigma_error("sigma_error", "sigma_error", sigma_control.getError());
  
  // Then we construct a constraining PDF using the RooGaussian class:
  // NOTE that paramerer "mean" and "sigma" are treated as observables!
  RooGaussian constrMeanPdf("constrMeanPdf","constrMeanPdf",mean, mean_bestValue, mean_error);
  RooGaussian constrSigmaPdf("constrSigmaPdf","constrSigmaPdf",sigma, sigma_bestValue, sigma_error);
  
  //---------------------------------   
  // perform fit
  //---------------------------------  
  // model.fitTo(*data, Extended(true));  
 model.fitTo(*data, Extended(true), ExternalConstraints(RooArgSet(constrMeanPdf, constrSigmaPdf)));  
  
  
  new TCanvas("c2", "Signal sample", 200, 200, 800, 600);
  RooPlot* frame2 = m.frame();
  
  // draw data and PDFs onto the frame1
  data ->plotOn(frame2, Binning(30));
  model.plotOn(frame2, LineColor(kBlue), LineWidth(3));
  frame2->Draw();
      
  cout << "ns    = " << ns.getVal() << " +/- " << ns.getError() << endl;
  cout << "sigma = " << sigma.getVal() << " +/- " << sigma.getError() << endl;

}
