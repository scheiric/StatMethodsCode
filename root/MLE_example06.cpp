/* Example 6: 2-dimensional fit and the likelihood contours */

#include "RooDataSet.h"
#include "RooRealVar.h"
#include "RooPolynomial.h"
#include "RooPlot.h"
#include "RooMinimizer.h"
#include "RooMinuit.h"
#include "TFile.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TMath.h"
#include "TH2D.h"

using namespace RooFit;
using namespace std;

void MLE_example06() {
  
  // define observable: RooFit class RooRealVar
  RooRealVar x("x", "x", 0, -0.95, 0.95); // value, range_min, range_max (N.B. the value doesn't matter here)
  
  // Load the TTree form the input file
  // NOTE: it is assumed that the "scatter.root" file is already created 
  //       by running lesson3_example03.cpp
  TFile* inFile = TFile::Open("scatter.root");
  TTree* t = (TTree*)inFile->Get("scatter");

  // convert data to the RooFit class RooDataSet. The imported tree must have a branch named "x"
  RooDataSet data("data", "data", t, RooArgSet(x));
  
  /////////////////////////////
  // define fit model:
  /////////////////////////////

  // Parameters: again RooRealVar classes
  RooRealVar alpha("alpha", "#alpha", 0.8, 0, 1); // initial value, range_min, range_max
  RooRealVar beta ("beta",  "#beta",  0.8, 0, 1); // initial value, range_min, range_max
  
  // model: using RooPolynomial class
  RooPolynomial model("model", "aaa", x, RooArgSet(alpha, beta));
  
  // perform the fit:
  model.fitTo(data, Save(true));
  
  // plot the result: using RooFit class RooPlot
  new TCanvas("c1", "ML fit", 200, 200, 800, 600);

  RooPlot* frame_final = x.frame();
  data .plotOn(frame_final, Binning(50), LineColor(kBlack), LineWidth(2), MarkerSize(1.2), XErrorSize(1.));
  model.plotOn(frame_final, LineColor(kBlue), LineWidth(3));

  frame_final->Draw();

  /////////////////////////////
  // Make negative log likelihood contour plot
  /////////////////////////////
  // Construct unbinned likelihood of model w.r.t. data
  RooAbsReal* nll = model.createNLL(data);
  
  // Create MINUIT interface object
  RooMinuit m(*nll) ;  
  
  // Make contour plot of alpha vs beta
  new TCanvas("c2", "logL contour plot", 200, 200, 800, 600);
  RooPlot* frame_contour = m.contour(alpha,beta,TMath::Sqrt(2.3), TMath::Sqrt(6.18)) ;
  frame_contour->SetTitle("RooMinuit contour plot") ; 
  frame_contour->Draw();
  
  // draw true value
  TGraph* g = new TGraph(1);
  g->SetPoint(0, 0.5, 0.5);
  g->SetMarkerStyle(2);
  g->Draw("p");
  
  /////////////////////////////
  // Generate pseudo-experiments with true alpha and beta
  /////////////////////////////
  // histogram to hold pseudo-results
  TH2D* h_pseudo = new TH2D("h_pseudo", "results of fits to the pseudo-experiments", 100, -2, 2, 100, -2, 2);
  
  int nexp = 10000;
  int nEvt = t->GetEntries(); // size of the sample 
  for(int i=0; i<nexp; ++i) {
    // reset parameters to the known true value
    alpha.setVal(0.5);
    beta .setVal(0.5);
    
    // generate pseudo-data using RooFit:
    RooDataSet* pseudo_data = model.generate(x, nEvt);
    
    // perform fit on pseudo-data
    model.fitTo(*pseudo_data);
    
    // fill the histogram
    h_pseudo->Fill(alpha.getVal(), beta.getVal());
  }
  
  // draw the histogram
  Double_t contours[1];
  contours[0] = h_pseudo->GetMaximum() * TMath::Exp(-0.5*2.3);
  h_pseudo->SetContour(1, contours);
  
  h_pseudo->Draw("same cont1");
  
}
