/* 
 * Example 1: likelihood function and ML fit
 * Run: root -l lesson3_example01.cpp+
 */

#include "TRandom3.h"
#include "TH1D.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TMath.h"
#include "TString.h"
#include "TText.h"
#include "TLegend.h"

#include <vector>

using namespace std;

vector <double> events;
double logL(double *xx, double *par);


// MAIN BODY OF THE EXAMPLE
void MLE_example01() {
  
  
  // define PDF: normalized gaussian pre-defined in ROOT "gausn(0)"
  TF1* f1 = new TF1("f1", "gausn(0)", 3, 17);
  f1->SetTitle(";x;Probability");
  f1->SetParameter(0, 1); // 1st parameter: constant -- must be 1 to have the gaussian normalized!
  f1->SetParameter(1, 10); // 2nd parameter: mean
  f1->SetParameter(2, 1);  // 3rd parameter: sigma

  // Define another PDF with different (wrong) parameters
  TF1* f2 = new TF1("f2", "gausn(0)", 3, 17);
  f2->SetLineColor(kBlue);
  f2->SetParameters(1, 7, 1); // constant, mean, sigma

  // Define  PDF we will fit usinhdata the maximum likelihood method
  TF1* f3 = new TF1("f3", "gausn(0)", 3, 17);
  f3->SetLineColor(kBlack);
  f3->SetParameters(1, 10, 1);  // constant, mean, sigma
  
  // Generate gaussian data:
  double true_mean  = 10; // generated distribution mean
  double true_sigma = 1;  // generated distribution sigma
  int    n          = 50; // no. of events
  
  TRandom3 rnd(1266);                     // random generator
  TH1D* hdata = new TH1D("hdata", "Data;x;", 10000, 2, 17);  // TGraph class to store unbinned data
  
  for( int i=0; i<n; ++i) {
     double x = rnd.Gaus(true_mean, true_sigma ); // generate normal distributed values
     hdata->Fill(x, 0.1); // store data 
     events.push_back(x); // store data for the logL function (see below)
  }
  
  // define likelihood function  (see end of the file for definition of logL)
  TF1 *fLogL = new TF1("fLogL", logL, 3, 17, 0); // 3..17 is the range of mean_hat in which the max logL is searched
  
  // calculate likelihood:
  double logL1 =  fLogL->Eval(f1->GetParameter(1)); // evaluate at the mean of the 1st PDF
  double logL2 =  fLogL->Eval(f2->GetParameter(1)); // evaluate at the mean of the 2nd PDF
  
  // For the 3rd PDF let's perform the ML fit.
  // a very simple way to maximize likehlihood:
  double mean_hat = fLogL->GetMaximumX();
  double logL3 = fLogL->Eval(mean_hat); // evaluate the logL_max
  f3->SetParameter(1, mean_hat); // pass the mean_hat to the fitted function
  std::cout << "Mean_hat = " << mean_hat << std::endl;

  // display PDF and events:
  TCanvas* c1 = new TCanvas("c1", "Random numbers");
  
  // display PDF
  f1->Draw();
  f2->Draw("same");
  f3->Draw("same");
  
  // display data
  hdata->Draw("same");
  
  // convert double to strinhdata for printing
  TString text1, text2, text3;

  text1.Form("log L = %.3f", logL1);
  text2.Form("log L = %.3f", logL2);
  text3.Form("log L = %.3f", logL3);

  
  // print on canvas:
  TLegend* legend = new TLegend(0.6, 0.5, 0.88, 0.88);
  legend->SetBorderSize(0);
  legend->AddEntry(f1, text1, "l");
  legend->AddEntry(f2, text2, "l");
  legend->AddEntry(f3, text3, "l");
  legend->SetFillColor(kWhite);
  legend->Draw("same");
  
  
  // update canvas
  c1->Modified();
  c1->Update();
  
}

// LIKELIHOOD FUNCTION
double logL(double *xx, double *par) {

  double mean  = xx[0];
  double sigma = 1;     // for the sake of simplicity we fix the sigma to the correct value

  double logL = 0;
  for( size_t i=0; i<events.size(); i++) {
    double & x = events[i];
    logL += TMath::Log(1./TMath::Sqrt(2*TMath::Pi())) + 0.5*TMath::Log(1./sigma/sigma) - 0.5*(x-mean)*(x-mean)/sigma/sigma; // logarithm of a gaussian
  }

  return logL;

}

