/* Homework: histogram fits with different models
 *
 * Run: root -l MLE_homework.cpp+
 * 
 */

#include "TH2D.h"
#include "TRandom3.h"
#include "TSystem.h"
#include "TCanvas.h"
#include <math.h>
#include "TStyle.h"
#include "TF1.h"
#include "TLine.h"
#include "TMath.h"

using namespace std;

// MAIN BODY OF THE EXAMPLE
void MLE_homework() {


   /////////////////////////////////////////////////////////////////////
   // generate data
   /////////////////////////////////////////////////////////////////////
   
   int n = 50;
   double tau = 1.062;

   TH1D *hdata = new TH1D("hdata", "hdata; t; events", 10, 0, 10);
   TRandom3 rnd(1266);
   for( int i=0; i<n; i++) {
      double x = rnd.Exp(tau);
      hdata -> Fill( x );
   }

   /////////////////////////////////////////////////////////////////////
   // DEFINE FITTED FUNCTION
   /////////////////////////////////////////////////////////////////////
   TF1* fun = new TF1("fun", "[1]*TMath::Exp(-x/[0])", 0, 10);
   // set starting point
   fun->SetParameter(0, 0.8); // mean lifetime. We "don't know" the value so just guess
   fun->FixParameter(1, double(n)*10/hdata->GetNbinsX());   // normalization factor. Will be fixed
   
   /////////////////////////////////////////////////////////////////////
   // HERE PUT YOUR SOLUTION
   /////////////////////////////////////////////////////////////////////
   // example solution for a)
   
   hdata->Fit(fun, "W");
   
   
   

}
