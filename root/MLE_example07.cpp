/* Example 2: binned maximum likelihood
 *
 * Fit histogram h with exponential using
 *      a) least square fit assuming all bin errors are 1
 *      b) least square fit assuming all bin errors are sqrt(n_i)
 *      c) binned maximum likelihood with poisson PDF
 *   - compare the result with unbinned ML fit from example 2
 *
 * Run: root -l lesson4_example02.cpp+
 * 
 */

#include "RooDataSet.h"
#include "RooRealVar.h"
#include "RooExponential.h"
#include "RooPlot.h"
#include "TFile.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TMath.h"
#include "TH2D.h"
#include "TF1.h"
#include "TString.h"
#include "TLegend.h"

using namespace std;
using namespace RooFit;

// MAIN BODY OF THE EXAMPLE
void MLE_example07() {

  /////////////////////////////////////////////////////////////////////
  // model
  /////////////////////////////////////////////////////////////////////

   RooRealVar t  ("t", "t", 0, 0, 5);  // observable
   RooRealVar Gamma("Gamma", "Gamma", -1.5, -10., 0.);  // parameter
   
   RooExponential model("model", "Exp(t*Gamma)", t, Gamma);

   /////////////////////////////////////////////////////////////////////
   // generate data
   /////////////////////////////////////////////////////////////////////
   int nEvt = 50;
   RooDataSet* data = model.generate(t, nEvt);
   
   /////////////////////////////////////////////////////////////////////
   // fit the model
   /////////////////////////////////////////////////////////////////////
   model.fitTo(*data);
   
   /////////////////////////////////////////////////////////////////////
   // display data
   /////////////////////////////////////////////////////////////////////
   // plot the result: using RooFit class RooPlot
   new TCanvas("c1", "Fit", 200, 200, 800, 600);

   RooPlot* frame_final = t.frame();
   data ->plotOn(frame_final, Binning(10), LineColor(kBlack), LineWidth(2), MarkerSize(1.2), XErrorSize(1.));
   model.plotOn(frame_final, LineColor(kBlue), LineWidth(3));

   frame_final->Draw();
   
   /////////////////////////////////////////////////////////////////////
   // binned fits in ROOT
   /////////////////////////////////////////////////////////////////////
   // convert RooDataSet to a ROOT histogram
   TH1D *hdata = new TH1D("hdata", "hdata; t; events", 10, 0, 5);
   data->fillHistogram(hdata, RooArgList(t));
   
   /////////////////////////////////////////////////////////////////////
   // DEFINE FITTED FUNCTIONS
   /////////////////////////////////////////////////////////////////////
   vector<TF1*> functions;
   for(int i=0; i<3; ++i) {
     TF1* fun = new TF1(TString("fun")+TString(i), "-[1]*[0]*TMath::Exp([0]*x)", 0, 5);
     // set starting point
     fun->SetParameter(0, -1.5); // mean lifetime. We "don't know" the value so just guess
     fun->FixParameter(1, double(nEvt)*5/hdata->GetNbinsX());   // normalization factor. Will be fixed
     functions.push_back( fun );
   }
   functions.push_back(new TF1(TString("fun4"), "-[1]*[0]*TMath::Exp([0]*x)", 0, 5));
   /////////////////////////////////////////////////////////////////////
   // PERFORM FITS
   /////////////////////////////////////////////////////////////////////
   hdata->Fit(functions[0], "W I 0");
   hdata->Fit(functions[1], "0 I" );
   hdata->Fit(functions[2], "L I 0");

   // draw functions:
   functions[0]->SetLineColor(kRed);
   functions[1]->SetLineColor(kMagenta);
   functions[2]->SetLineColor(kCyan);
   functions[3]->SetLineColor(kBlue);
   functions[3]->SetLineWidth(3);
   
   for(int i=0; i<3; ++i) {
     functions[i]->Draw("same");
   }
   
   // Draw legend
   TLegend* leg = new TLegend(0.5, 0.85, 0.85, 0.4);
   leg->SetFillColor(kWhite);
   leg->SetTextFont(42);
   leg->SetTextSize(0.05);
   leg->AddEntry(functions[0], "Chi2 fit all weights 1", "l");
   leg->AddEntry(functions[1], "Chi2 fit", "l");
   leg->AddEntry(functions[2], "Binned ML fit", "l");
   leg->AddEntry(functions[3], "Unbinned ML fit", "l");
   leg->Draw("same");
   
   cout << "Chi2 fit all weights 1: " << functions[0]->GetParameter(0) << "+/-" <<  functions[0]->GetParError(0) << endl; 
   cout << "Chi2 fit              : " << functions[1]->GetParameter(0) << "+/-" <<  functions[1]->GetParError(0) << endl; 
   cout << "Binned ML fit         : " << functions[2]->GetParameter(0) << "+/-" <<  functions[2]->GetParError(0) << endl; 
   cout << "Unbinned ML fit       : " << Gamma.getVal() << "+/-" <<  Gamma.getError() << endl; 

}
