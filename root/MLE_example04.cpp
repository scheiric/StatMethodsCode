/* Example 4: similar to example 3 but in RooFit */

#include "RooDataSet.h"
#include "RooRealVar.h"
#include "RooPolynomial.h"
#include "RooPlot.h"
#include "RooMinimizer.h"
#include "RooMinuit.h"
#include "TFile.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TH2D.h"
#include "TLine.h"
#include "TLegend.h"

using namespace RooFit;
using namespace std;

void MLE_example04() {
  
  // define observable: RooFit class RooRealVar
  RooRealVar x("x", "x", 0, -0.95, 0.95); // value, range_min, range_max (N.B. the value doesn't matter here)
  
  // Load the TTree form the input file
  // NOTE: it is assumed that the "scatter.root" file is already created 
  //       by running lesson3_example03.cpp
  TFile* inFile = TFile::Open("scatter.root");
  TTree* t = (TTree*)inFile->Get("scatter");

  // convert data to the RooFit class RooDataSet. The imported tree must have a branch named "x"
  RooDataSet data("data", "data", t, RooArgSet(x));
  
  /////////////////////////////
  // define fit model:
  /////////////////////////////

  // Parameters: again RooRealVar classes
  RooRealVar alpha("alpha", "#alpha", 0.8, -2, 2); // initial value, range_min, range_max
  RooRealVar beta ("beta",  "#beta",  0.8, -2, 2); // initial value, range_min, range_max
  
  // model: using RooPolynomial class
  RooPolynomial model("model", "1+alpha*x + beta*x^2", x, RooArgSet(alpha, beta));
  
  // perform the fit:
  model.fitTo(data, Save(true));
  
  // plot the result: using RooFit class RooPlot
  new TCanvas("c1", "ML fit", 0, 10, 800, 600);

  RooPlot* frame_final = x.frame();
  data .plotOn(frame_final, Binning(50), LineColor(kBlack), LineWidth(2), MarkerSize(1.2), XErrorSize(1.));
  model.plotOn(frame_final, LineColor(kBlue), LineWidth(3));

  frame_final->Draw();

  /////////////////////////////
  // Make negative log likelihood contour plot
  /////////////////////////////
  cout << "Contour plot --------------------------------------------------------------" << endl;
  // Construct unbinned likelihood of model w.r.t. data
  RooAbsReal* nll = model.createNLL(data);
  
  // Create MINUIT interface object
  // RooMinimizer m(*nll) ;  
  RooMinuit m(*nll) ;  
  
  // Run HESSE and to calculate errors from d2L/dp2
  m.migrad();
  m.hesse();

  // Make contour plot of alpha vs beta at 1,2,3 sigma
  new TCanvas("c2", "logL contour plot", 0, 10, 800, 600);
  TH2D* axs = new TH2D("axs", ";#alpha;#beta", 100, 0.3, 0.9, 100, -0.5, 1.3 );
  axs->SetStats(0);
  axs->SetLineColor(kBlue);
  axs->SetLineWidth(2);
  axs->SetMarkerStyle(20);
  axs->Draw();
  RooPlot* frame_contour = m.contour(alpha,beta) ;
  frame_contour->SetTitle("RooMinuit contour plot") ; 
  frame_contour->Draw("same");
  
  // draw true value
  TGraph* g = new TGraph(1);
  g->SetPoint(0, 0.5, 0.5);
  g->SetMarkerSize(3);
  g->SetLineWidth(2);
  g->SetMarkerStyle(2);
  g->Draw("p");
  
  /////////////////////////////
  // 1D error bars
  /////////////////////////////
  std::cout<< std::endl<< std::endl<< std::endl;  
  std::cout << "1D errors:" << std::endl;
  std::cout << "  alpha = " << alpha.getVal() << " +/- " << alpha.getError() << std::endl;
  std::cout << "  beta  = " << beta.getVal()  << " +/- " << beta.getError() << std::endl;
  
  double a  = alpha.getVal();
  double ae = alpha.getError();
  double b  = beta.getVal();
  double be = beta.getError();
  std::vector<TLine*> errorBox;
  errorBox.push_back(new TLine(a-ae, b-be, a+ae, b-be));
  errorBox.push_back(new TLine(a-ae, b+be, a+ae, b+be));
  errorBox.push_back(new TLine(a-ae, b-be, a-ae, b+be));
  errorBox.push_back(new TLine(a+ae, b-be, a+ae, b+be));
  for(std::vector<TLine*>::iterator line = errorBox.begin();
      line!=errorBox.end();
      ++line)
  {
    (*line)->SetLineStyle(5);
    (*line)->Draw();
  }
  
  // draw legend
  TLegend* l = new TLegend(0.2, 0.2, 0.89, 0.43);
  l->SetTextFont(42);
  l->SetTextSize(0.05);
  l->AddEntry(g, "Input values", "p");
  l->AddEntry(axs, "ML estimate", "p");
  l->AddEntry(axs, "Likelihood contour", "l");
  l->AddEntry(errorBox[0], "1D error box", "l");
  l->SetFillColor(kWhite);
  l->Draw("same");
  
  
}
