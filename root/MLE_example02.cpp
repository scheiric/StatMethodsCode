/* 
 * Example 2: uncertainty of the ML estimator
 * Run: root -l lesson3_example02.cpp+
 */

#include "TH2D.h"
#include "TRandom3.h"
#include "TSystem.h"
#include "TCanvas.h"
#include <math.h>
#include "TStyle.h"
#include "TF1.h"
#include "TLine.h"
#include "TMath.h"
#include "TLegend.h"


using namespace std;
vector <double> events;
double logL(double *xx, double *par);

// MAIN BODY OF THE EXAMPLE
void MLE_example02() {


   gStyle-> SetOptStat(1101);


   /////////////////////////////////////////////////////////////////////
   // generate data
   /////////////////////////////////////////////////////////////////////
   
   int n = 50;
   double tau = 1.062;
   cout << "Input value of tau: " << tau << endl; 

   TH1D *hdata = new TH1D("hdata", "hdata; t; Events", 10, 0, 10);
   hdata->SetStats(0);
   TRandom3 rnd(1266);

   double sample_mean = 0;  // calculate sample mean
   
   for( int i=0; i<n; i++) {

      double x = rnd.Exp(tau);
      events.push_back( x );

      hdata -> Fill( x );
      sample_mean += x;
   }
   
   sample_mean /= n;  // calculate sample mean
   cout << "Sample mean: " << sample_mean << endl; 


   //////////////////////////////////////////////////////////////////// 
   // define the liklihood function
   TF1 *fLogL = new TF1("logL", logL, 0.1, 1.9, 0);
   fLogL->SetTitle(";t;log L");

   // a very simple way to maximize likehlihood
   double tau_measured = fLogL-> GetMaximumX();
   cout << "ML estimate of tau: " << tau_measured << endl; 
   
   new TCanvas("c1", "Data and the ML fit");
   hdata-> Draw();
   
   // display exponential function with the ML fitted tau:
   TF1* fExp = new TF1("fExp", "1./[0]*TMath::Exp(-x/[0])*[1]", 0, 10);
   fExp->SetLineColor(kBlue);
   fExp->SetParameter(0, tau_measured); // set tau to the ML extimator
   fExp->SetParameter(1, hdata->Integral()); // normalize to the histogram area
   fExp->Draw("same");
   
   // legend
   TLegend* l = new TLegend(0.5, 0.7, 0.85, 0.85);
   l->SetTextFont(42);
   l->SetTextSize(0.05);
   l->SetFillColor(kWhite);
   l->AddEntry(hdata, "Data", "l");
   l->AddEntry(fExp , "ML fit", "l");
   l->Draw("same");
   
   // plot the likelihood function onto a canvas
   new TCanvas("c2", "logL function");
   TH2D* axs = new TH2D("axs",";t;log L", 100, 0.8, 1.8, 100, -61, -54);
   axs->SetStats(0);
   axs->Draw();
   for(int i=0; i<100; ++i) {
     TLine* l = new TLine(0.8+i*0.01, fLogL->Eval(0.8+i*0.01), 0.8+(i+1)*0.01, fLogL->Eval(0.8+(i+1)*0.01));
     l->SetLineColor(kRed);
     l->SetLineWidth(3);
     l->Draw();
   }

   // calculate variance analytically: sigma^2 = tau_measured^2 / n
   cout << "Analytically estimated sigma[mean_tau]: " << tau_measured/TMath::Sqrt(n) << endl; 
   
   // calculate variance using RCF bound: second derivative of logL at mean = mean_measured
   double d2LogL_over_dTau2 = fLogL->Derivative2(tau_measured);
   cout << "Sigma[mean_tau] from RCF bound: " << 1./TMath::Sqrt(-d2LogL_over_dTau2) << endl; 
   
   // calculate variance using graphical method: solve logL(tau +/- sigmaTau) = logL_max - 1/2
   double logL_max = fLogL->Eval(tau_measured);
   
   // find the lower bound
   double tau_minus = fLogL->GetX(logL_max-0.5, 0.8, tau_measured); // find the solution in range 0.8 .. tau_measured
   // find the upper bound
   double tau_plus = fLogL->GetX(logL_max-0.5, tau_measured, 1.8); // find the solution in range tau_measured .. 1.8
   cout << "Sigma[mean_tau] from the graphical method: " << 
     " sigma- = " << tau_measured-tau_minus << 
     ", sigma+ = " << tau_plus-tau_measured << 
     ", average = " << (tau_plus-tau_minus)*0.5 << endl; 
   
   //draw lines
   TLine* l1 = new TLine(tau_minus, logL_max-0.5, tau_plus, logL_max-0.5);
   l1->SetLineStyle(2);
   l1->Draw();

   TLine* l2 = new TLine(tau_minus, logL_max-0.5, tau_minus, -61);
   l2->SetLineStyle(2);
   l2->Draw();
   
   TLine* l3 = new TLine(tau_plus, logL_max-0.5, tau_plus, -61);
   l3->SetLineStyle(2);
   l3->Draw();

   TLine* l4 = new TLine(tau_measured, logL_max, tau_measured, -61);
   l4->SetLineStyle(2);
   l4->Draw();
   
   //////////////////////////////////////////////////////////////////// 
   // pseudo experiemnts
   //////////////////////////////////////////////////////////////////// 
   int nexp = 10000;
   
   TH1D *h_exper = new TH1D("h_exper", "ML estimates #hat{#tau} for 10000 experiments with 50 observations;#hat{#tau};Pseudo-experiments", 100, 0, 2);
   h_exper->SetStats(0);
   for(int i=0; i< nexp; i++) {
   
      events.clear();
   
      for( int i=0; i<n; i++) {
   
         double x = rnd.Exp(tau_measured);
         events.push_back( x );
      }
   
      h_exper -> Fill( fLogL-> GetMaximumX() );
   }
     
   new TCanvas("c3", "pseudo experiments");
   h_exper-> Draw();
   
   // calculate variance from pseudoexperiments:
   cout << "sigma[mean_tau] from pseudo experiments: " << h_exper->GetRMS() << endl; 

}

// likelihood function for know exponential distribution
double logL(double *xx, double *par)  {

   double tau = xx[0];


   double logL = 0;

   for( uint i=0; i<events.size(); i++) {

      double & x = events[i];
     
      logL += log(1/tau) - x/tau;
   }

   return logL;

}
