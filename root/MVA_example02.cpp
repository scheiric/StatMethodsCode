/*
 * Example of the TMVA package (Multivariate analysis)
 * for event clasification
 *
 * Make sure you run the MVA_example01.cpp beforehand
*
 * Example 2: application of the trained MVA clasifiers on testing MC sample
 *
 * Execute by : root -l lesson10_example02.cpp+
 */

#include <cstdlib>
#include <iostream>
#include <map>
#include <string>

#include "TChain.h"
#include "TFile.h"
#include "TString.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TString.h"
#include "TObjString.h"
#include "TSystem.h"
#include "TROOT.h"
#include "TMath.h"
#include "TLegend.h"
#include "TH2F.h"
#include "TLine.h"

#include "TMVA/Tools.h"
#include "TMVA/Reader.h"
#include "TMVA/MethodCuts.h"

using namespace TMVA;

// main function
void MVA_example02() {

  // This loads the library
  TMVA::Tools::Instance();

  // --- Create the Reader object
  TMVA::Reader *reader = new TMVA::Reader( "!Color:!Silent" );    
  
  // Create a set of variables and declare them to the reader
  // - the variable names MUST corresponds in name and type to those given in the weight file(s) used
  float fChi2, lxySig, a0Sig, pt;
  reader->AddVariable( "chi2",   &fChi2 );
  reader->AddVariable( "lxySig", &lxySig );
  reader->AddVariable( "a0Sig",  &a0Sig );
  reader->AddVariable( "pt",     &pt );
  
  // Spectator variables declared in the training have to be added to the reader, too
  float fMass;
  reader->AddSpectator( "mass",   &fMass );
  
  // --- Book the MVA methods
  reader->BookMVA( "Cuts method", "dataset/weights/TMVAClassification_Cuts.weights.xml" ); 
  reader->BookMVA( "Fisher method", "dataset/weights/TMVAClassification_Fisher.weights.xml" ); 
  reader->BookMVA( "BDT method", "dataset/weights/TMVAClassification_BDT.weights.xml" ); 
  reader->BookMVA( "MLP method", "dataset/weights/TMVAClassification_MLP.weights.xml" ); 
  
  // Book output histograms
  UInt_t nbin = 100;
  
  // MVA output
  TH1F* histFi_S  = new TH1F( "MVA_Fisher_S",        "MVA_Fisher",        nbin, -1, 1 );
  TH1F* histBdt_S = new TH1F( "MVA_BDT_S",           "MVA_BDT",           nbin, -1, 0.4 );
  TH1F* histNn_S  = new TH1F( "MVA_MLP_S",           "MVA_MLP",           nbin, 0, 1 );
  
  TH1F* histFi_B  = new TH1F( "MVA_Fisher_B",        "MVA_Fisher",        nbin, -1, 1 );
  TH1F* histBdt_B = new TH1F( "MVA_BDT_B",           "MVA_BDT",           nbin, -1, 0.4 );
  TH1F* histNn_B  = new TH1F( "MVA_MLP_B",           "MVA_MLP",           nbin, 0, 1 );
  
  // read DATA file
  TFile* input = TFile::Open( "skimThin.mc10.Lam+LamBar.MinBias.SimpleTree.01.root" );
  TTree* t     = (TTree*)input->Get("Lam");  // TTree for Lam->p+pi decays

  // setup tree branches  
  vector<double> *chi2 = 0, *px = 0, *py = 0, *mass = 0;
  vector<vector<double> > *lxy = 0, *lxyError = 0, *a0 = 0, *a0Error = 0;
  vector<int> *isSignal = 0;
  t->SetBranchAddress("VTX_chi2"    , &chi2);
  t->SetBranchAddress("VTX_lxy"     , &lxy);
  t->SetBranchAddress("VTX_lxyError", &lxyError);
  t->SetBranchAddress("VTX_a0"      , &a0);
  t->SetBranchAddress("VTX_a0Error" , &a0Error);
  t->SetBranchAddress("VTX_px"      , &px);
  t->SetBranchAddress("VTX_py"      , &py);
  t->SetBranchAddress("VTX_mass"    , &mass);
  t->SetBranchAddress("isSignal"    , &isSignal);
  
  // loop over events in the input tree
  for(int i=t->GetEntries()/2; i<t->GetEntries(); ++i) {
    if(i%10000==0) cout<< "event " << i <<endl;
    
    // read event    
    t->GetEntry(i);
    
    // loop over Lam candidates in each single event
    for(uint j=0; j<chi2->size(); ++j) {
      // fill the TMVA variables
      fChi2   = chi2->at(j);                                                  // vertex fit min chi2
      lxySig =  lxy->at(j)[0]/lxyError->at(j)[0];                            // decay distance significance
      a0Sig  = a0->at(j)[0]/a0Error->at(j)[0];                               // impact parameter significance
      pt     = TMath::Sqrt(px->at(j) * px->at(j) + py->at(j) * py->at(j));   // transverse momentum
      fMass   = mass->at(j);                                                  // reconstructed mass
      
      // --- Return the MVA outputs and fill into histograms
      if(isSignal->at(j)) {
        // event is signal
        histFi_S   ->Fill( reader->EvaluateMVA( "Fisher method") );
        histBdt_S  ->Fill( reader->EvaluateMVA( "BDT method"   ) );
        histNn_S   ->Fill( reader->EvaluateMVA( "MLP method"   ) );
      }else{
        // event is background
        histFi_B   ->Fill( reader->EvaluateMVA( "Fisher method") );
        histBdt_B  ->Fill( reader->EvaluateMVA( "BDT method"   ) );
        histNn_B   ->Fill( reader->EvaluateMVA( "MLP method"   ) );
      }
                
    } // end of loop over Lam candidates
  } // end of loop over events

  // display score distributions
  new TCanvas("c1", "Fisher discriminant", 200, 200, 800, 600);
  histFi_S->SetLineColor(kGreen);
  histFi_B->SetLineColor(kRed);
  histFi_S->Draw();
  histFi_B->Draw("same");

  new TCanvas("c2", "BDT", 200, 200, 800, 600);
  histBdt_S->SetLineColor(kGreen);
  histBdt_B->SetLineColor(kRed);
  histBdt_S->Draw();
  histBdt_B->Draw("same");

  new TCanvas("c3", "MLP", 200, 200, 800, 600);
  histNn_S->SetLineColor(kGreen);
  histNn_B->SetLineColor(kRed);
  histNn_S->Draw();
  histNn_B->Draw("same");
}
