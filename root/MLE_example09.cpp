/*
 * example of extended likelihood fit
 */

#include "RooDataSet.h"
#include "RooRealVar.h"
#include "RooGaussian.h"
#include "RooChebychev.h"
#include "RooAddPdf.h"
#include "RooPlot.h"
#include "RooDataSet.h"
#include "TCanvas.h"
#include "TH1D.h"

using namespace RooFit;

// main function
void MLE_example09() {
  
  int nEvt = 300;
  
  RooRealVar m("m", "m", 0, 100, 150, "GeV"); // mass

  //---------------------------------  
  // define model parameters. Again, class RooRealVar is used.
  //---------------------------------  
  
  // examples:
  RooRealVar mean ("mean", "#mu", 125, 80, 200, "GeV"); // if no range is given, the value is fixed
  RooRealVar sigma("sigma", "#sigma", 2., 0, 10);
  RooRealVar fSig ("fSig", "f_{sig}", 0.2, 0, 1);
  RooRealVar slope("slope", "a_{1}", -0.8, -1, 1);
  
  RooRealVar ns ("ns", "ns", fSig.getVal()*nEvt, 0, 1000000);
  RooRealVar nb ("nb", "nb", (1-fSig.getVal())*nEvt, 0, 1000000);
  
  RooGaussian  sigPdf("sigPdf", "sigPdf", m, mean, sigma);
  RooChebychev bkgPdf("bkgPdf", "bkgPdf", m, RooArgSet(slope));
  
  RooAddPdf model1("model1", "f sigPdf + (1-f) bkgPdf", RooArgSet(sigPdf, bkgPdf), RooArgSet(fSig));
  RooAddPdf model2("model2", "ns sigPdf + nb bkgPdf", RooArgSet(sigPdf, bkgPdf), RooArgSet(ns,nb));

  //---------------------------------  
  // generate toy data
  //---------------------------------  
  RooDataSet* data = model1.generate(m, nEvt);
  
  //---------------------------------   
  // display initial PDFs
  //---------------------------------  
  
  // create canvas (ordinary root TCanvas)
  new TCanvas("c1", "initial PDF", 200, 200, 800, 600);
  
  // create RooPlot class. This class can display data and PDFs on top of each other
  RooPlot* frame1 = m.frame(); // NOTE: x.frame() method returns POINTER!
  
  // draw data and PDFs onto the frame1
  data ->plotOn(frame1, Binning(30));
  model1.plotOn(frame1, LineColor(kRed), LineWidth(3));
  
  // plot the PDF's constituents, i.e. gaussian and polynomial
  frame1->Draw(); // draw frame1 onto the root canvas
    
  //---------------------------------   
  // perform fit
  //---------------------------------  
  model1.fitTo(*data);
  model2.fitTo(*data, Extended(true));

  //---------------------------------  
  // display
  //---------------------------------  
  new TCanvas("c2", "fitted PDF (non-extended)", 200, 200, 800, 600);
  RooPlot* frame2 = m.frame();
  
  // draw data and PDFs onto the frame1
  data ->plotOn(frame2, Binning(30));
  model1.plotOn(frame2, LineColor(kBlue), LineWidth(3));
  frame2->Draw();

  //---------------------------------  
  // display
  //---------------------------------  
  new TCanvas("c3", "fitted PDF (extended)", 200, 200, 800, 600);
  RooPlot* frame3 = m.frame();
  
  // draw data and PDFs onto the frame1
  data ->plotOn(frame3, Binning(30));
  model2.plotOn(frame3, LineColor(kBlue), LineWidth(3));
  frame3->Draw();

  cout << "fSig = " << fSig.getVal() << " +/- " << fSig.getError() << endl;
  cout << "Calculated Ns   = " << nEvt*fSig.getVal() << " +/- " << nEvt*fSig.getError() << endl;
  cout << "Calculated Nb   = " << nEvt*(1-fSig.getVal()) << " +/- " << nEvt*fSig.getError() << endl;

  // from extended likelihood fit
  cout << "ns = " << ns.getVal() << " +/- " << ns.getError() << endl;
  cout << "nb = " << nb.getVal() << " +/- " << nb.getError() << endl;

}
