/*
 * Example of the TMVA package (Multivariate analysis)
 * for event clasification
 *
 * Example 3: application of the trained MVA clasifiers on real DATA
 *
 * Execute by : root -l MVA_example03.cpp+
 */

#include <cstdlib>
#include <iostream>
#include <map>
#include <string>

#include "TChain.h"
#include "TFile.h"
#include "TString.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TString.h"
#include "TObjString.h"
#include "TSystem.h"
#include "TROOT.h"
#include "TMath.h"
#include "TLegend.h"
#include "TH2F.h"

#include "TMVA/Tools.h"
#include "TMVA/Reader.h"
#include "TMVA/MethodCuts.h"

using namespace TMVA;

// main function
void MVA_example03() {

  // This loads the library
  TMVA::Tools::Instance();

  // --- Create the Reader object
  TMVA::Reader *reader = new TMVA::Reader( "!Color:!Silent" );    
  
  // Create a set of variables and declare them to the reader
  // - the variable names MUST corresponds in name and type to those given in the weight file(s) used
  float fChi2, lxySig, a0Sig, pt;
  reader->AddVariable( "chi2",   &fChi2 );
  reader->AddVariable( "lxySig", &lxySig );
  reader->AddVariable( "a0Sig",  &a0Sig );
  reader->AddVariable( "pt",     &pt );
  
  // Spectator variables declared in the training have to be added to the reader, too
  float fMass;
  reader->AddSpectator( "mass",   &fMass );
  
  // --- Book the MVA methods
  reader->BookMVA( "Cuts method",   "dataset/weights/TMVAClassification_Cuts.weights.xml" ); 
  reader->BookMVA( "Fisher method", "dataset/weights/TMVAClassification_Fisher.weights.xml" ); 
  reader->BookMVA( "BDT method",    "dataset/weights/TMVAClassification_BDT.weights.xml" ); 
  reader->BookMVA( "MLP method",    "dataset/weights/TMVAClassification_MLP.weights.xml" ); 
  
  // mass distributions:
  TH1F* histAll_mass  = new TH1F( "mass", "All events;m (MeV);N", 35, 1100, 1132 );
  TH1F* histCuts_mass = new TH1F( "histCuts_mass", "MVA_Cuts events;m (MeV);N", 35, 1100, 1132 );
  TH1F* histFi_mass   = new TH1F( "mass_Fisher", "MVA_Fisher;m (MeV);N", 35, 1100, 1132 );
  TH1F* histBdt_mass  = new TH1F( "mass_BDT", "MVA_BDT;m (MeV);N", 35, 1100, 1132 );
  TH1F* histNn_mass   = new TH1F( "mass_MLP", "MVA_MLP;m (MeV);N", 35, 1100, 1132 );

  TH2F* pt_vs_a0Sig_Cuts_S   = new TH2F( "pt_vs_a0Sig_Cuts_S", "MVA_CUTS;p_{T} [MeV];a_{0}/#sigma;N", 100, 0,5000, 100, 0,3 );
  TH2F* pt_vs_a0Sig_Cuts_B   = new TH2F( "pt_vs_a0Sig_Cuts_B", "MVA_CUTS;p_{T} [MeV];a_{0}/#sigma;N", 100, 0,5000, 100, 0,3 );
  TH2F* pt_vs_a0Sig_Fi_S     = new TH2F( "pt_vs_a0Sig_Fi_S"  , "MVA_Fi;p_{T} [MeV];a_{0}/#sigma;N"  , 100, 0,5000, 100, 0,3 );
  TH2F* pt_vs_a0Sig_Fi_B     = new TH2F( "pt_vs_a0Sig_Fi_B"  , "MVA_Fi;p_{T} [MeV];a_{0}/#sigma;N"  , 100, 0,5000, 100, 0,3 );
  TH2F* pt_vs_a0Sig_BDT_S    = new TH2F( "pt_vs_a0Sig_BDT_S" , "MVA_BDT;p_{T} [MeV];a_{0}/#sigma;N" , 100, 0,5000, 100, 0,3 );
  TH2F* pt_vs_a0Sig_BDT_B    = new TH2F( "pt_vs_a0Sig_BDT_B" , "MVA_BDT;p_{T} [MeV];a_{0}/#sigma;N" , 100, 0,5000, 100, 0,3 );
  TH2F* pt_vs_a0Sig_MLP_S    = new TH2F( "pt_vs_a0Sig_MLP_S" , "MVA_MLP;p_{T} [MeV];a_{0}/#sigma;N" , 100, 0,5000, 100, 0,3 );
  TH2F* pt_vs_a0Sig_MLP_B    = new TH2F( "pt_vs_a0Sig_MLP_B" , "MVA_MLP;p_{T} [MeV];a_{0}/#sigma;N" , 100, 0,5000, 100, 0,3 );

  // plots for MC
  TH2F* pt_vs_a0Sig_MC_S    = new TH2F( "pt_vs_a0Sig_MC_S" , "MVA_MC;p_{T} [MeV];a_{0}/#sigma;N" , 100, 0,5000, 100, 0,3 );
  TH2F* pt_vs_a0Sig_MC_B    = new TH2F( "pt_vs_a0Sig_MC_B" , "MVA_MC;p_{T} [MeV];a_{0}/#sigma;N" , 100, 0,5000, 100, 0,3 );

  
  // read DATA file (this could be the real data file, but we will use our MC file again)
  TFile* input = TFile::Open( "data/skimThin.mc10.Lam+LamBar.MinBias.SimpleTree.01.root" );
  TTree* t     = (TTree*)input->Get("Lam");  // TTree for Lam->p+pi decays

  // setup tree branches  
  vector<double> *chi2 = 0, *px = 0, *py = 0, *mass = 0;
  vector<vector<double> > *lxy = 0, *lxyError = 0, *a0 = 0, *a0Error = 0;
  t->SetBranchAddress("VTX_chi2"    , &chi2);
  t->SetBranchAddress("VTX_lxy"     , &lxy);
  t->SetBranchAddress("VTX_lxyError", &lxyError);
  t->SetBranchAddress("VTX_a0"      , &a0);
  t->SetBranchAddress("VTX_a0Error" , &a0Error);
  t->SetBranchAddress("VTX_px"      , &px);
  t->SetBranchAddress("VTX_py"      , &py);
  t->SetBranchAddress("VTX_mass"    , &mass);
  
  // loop over events in the input tree
  for(int i=0; i<t->GetEntries(); ++i) {
    if(i%10000==0) cout<< "event " << i <<endl;
    
    // abort after 200000 events so we don't have to wait so long
    if(i>200000) break;

    // read event    
    t->GetEntry(i);
    
    // loop over Lam candidates in each single event
    for(uint j=0; j<chi2->size(); ++j) {
      // fill the TMVA variables
      fChi2   = chi2->at(j);                                                  // vertex fit min chi2
      lxySig =  lxy->at(j)[0]/lxyError->at(j)[0];                            // decay distance significance
      a0Sig  = a0->at(j)[0]/a0Error->at(j)[0];                               // impact parameter significance
      pt     = TMath::Sqrt(px->at(j) * px->at(j) + py->at(j) * py->at(j));   // transverse momentum
      fMass   = mass->at(j);                                                  // reconstructed mass
      
      // --- fill the mass distribution:
      histAll_mass->Fill(fMass);
      
      // optimized rectangular cuts:
      if(fChi2<1.6443 && lxySig>8.10443 && a0Sig < 1.78863 && pt > 905.411) {
        histCuts_mass->Fill(fMass);
        pt_vs_a0Sig_Cuts_S->Fill(pt, a0Sig);
      }else{
        pt_vs_a0Sig_Cuts_B->Fill(pt, a0Sig);
      }
      
      // apply cut on the value of the Fisher discriminant.
      if(reader->EvaluateMVA( "Fisher method") > -0.021 ) {
        histFi_mass->Fill(fMass);
        pt_vs_a0Sig_Fi_S->Fill(pt, a0Sig);
      } else {
        pt_vs_a0Sig_Fi_B->Fill(pt, a0Sig);
      }

      // apply cut on the output from the boosted decision tree
      if(reader->EvaluateMVA( "BDT method") > 0.057 ) {
        histBdt_mass->Fill(fMass);
        pt_vs_a0Sig_BDT_S->Fill(pt, a0Sig);
      } else {
        pt_vs_a0Sig_BDT_B->Fill(pt, a0Sig);
      }
      
      // apply cut on the output from the artifitial neural network
      if(reader->EvaluateMVA( "MLP method") > 0.593 ) {
        histNn_mass->Fill(fMass);
        pt_vs_a0Sig_MLP_S->Fill(pt, a0Sig);
      } else {
        pt_vs_a0Sig_MLP_B->Fill(pt, a0Sig);
      }
            
    } // end of loop over Lam candidates
  } // end of loop over events

  
  
  // read MC file
  TFile* inputMC = TFile::Open( "data/skimThin.mc10.Lam+LamBar.MinBias.SimpleTree.01.root" );
  TTree* tMC     = (TTree*)inputMC->Get("Lam");  // TTree for Lam->p+pi decays

  // setup tree branches  
  vector<int>* isSignal = 0;
  tMC->SetBranchAddress("VTX_chi2"    , &chi2);
  tMC->SetBranchAddress("VTX_lxy"     , &lxy);
  tMC->SetBranchAddress("VTX_lxyError", &lxyError);
  tMC->SetBranchAddress("VTX_a0"      , &a0);
  tMC->SetBranchAddress("VTX_a0Error" , &a0Error);
  tMC->SetBranchAddress("VTX_px"      , &px);
  tMC->SetBranchAddress("VTX_py"      , &py);
  tMC->SetBranchAddress("VTX_mass"    , &mass);
  tMC->SetBranchAddress("isSignal"    , &isSignal);
  
  // loop over events in the input tree
  for(int i=0; i<tMC->GetEntries(); ++i) {
    if(i%10000==0) cout<< "MC event " << i <<endl;
    
    // abort after 200000 events so we don't have to wait so long
    if(i>200000) break;

    // read event    
    tMC->GetEntry(i);
    
    // loop over Lam candidates in each single event
    for(uint j=0; j<chi2->size(); ++j) {
      // fill the TMVA variables
      fChi2   = chi2->at(j);                                                  // vertex fit min chi2
      lxySig =  lxy->at(j)[0]/lxyError->at(j)[0];                            // decay distance significance
      a0Sig  = a0->at(j)[0]/a0Error->at(j)[0];                               // impact parameter significance
      pt     = TMath::Sqrt(px->at(j) * px->at(j) + py->at(j) * py->at(j));   // transverse momentum
      fMass   = mass->at(j);                                                  // reconstructed mass
      
      // signal/background clasification based on MC truth:
      if(isSignal->at(j)) {
        pt_vs_a0Sig_MC_S->Fill(pt, a0Sig);
      }else{
        pt_vs_a0Sig_MC_B->Fill(pt, a0Sig);
      }
    }  
  }  
  
  // display mass distribution
  new TCanvas("c1", "All events", 200, 200, 800, 600);
  histAll_mass->SetMinimum(0);
  histAll_mass->Draw();
  
  new TCanvas("c2", "Mass of selected events", 200, 200, 800, 600);
  histCuts_mass->SetLineColor(kBlack);
  histCuts_mass->SetLineWidth(3);
  
  histFi_mass->SetLineColor(kRed);
  histFi_mass->SetLineWidth(3);
  
  histBdt_mass->SetLineColor(kMagenta);
  histBdt_mass->SetLineWidth(3);
  histBdt_mass->SetLineStyle(3);
  
  histNn_mass->SetLineColor(kGreen-2);
  histNn_mass->SetLineWidth(3);
  
  histNn_mass->Draw();
  histCuts_mass->Draw("same");
  histFi_mass->Draw("same");
  histBdt_mass->Draw("same");
  
  TLegend* l = new TLegend(0.1,0.9,0.35,0.6);
  l->AddEntry(histCuts_mass , "Cuts", "l");
  l->AddEntry(histFi_mass , "Fisher", "l");
  l->AddEntry(histBdt_mass , "BDT", "l");
  l->AddEntry(histNn_mass , "ANN", "l");
  
  l->SetFillColor(kWhite);
  l->Draw("same");
 
  // draw regions in lxySig and a0Sig space which were selected as signal and background:
  TCanvas* c4 = new TCanvas("c4", "TMVA selection regions", 200, 200, 800, 600);
  c4->Divide(3,2);

  // MC truth selection  
  c4->cd(1);
  pt_vs_a0Sig_MC_S->SetMarkerColor(kGreen);
  pt_vs_a0Sig_MC_B->SetMarkerColor(kRed);
  pt_vs_a0Sig_MC_S->SetMarkerStyle(1);
  pt_vs_a0Sig_MC_B->SetMarkerStyle(1);
  pt_vs_a0Sig_MC_B->Draw();
  pt_vs_a0Sig_MC_S->Draw("same");
  
  c4->cd(2);
  // for rectangular cuts
  pt_vs_a0Sig_Cuts_S->SetMarkerColor(kGreen);
  pt_vs_a0Sig_Cuts_B->SetMarkerColor(kRed);
  pt_vs_a0Sig_Cuts_S->SetMarkerStyle(1);
  pt_vs_a0Sig_Cuts_B->SetMarkerStyle(1);
  pt_vs_a0Sig_Cuts_B->Draw();
  pt_vs_a0Sig_Cuts_S->Draw("same");

  // for Fisher's discriminant
  c4->cd(3);
  pt_vs_a0Sig_Fi_S->SetMarkerColor(kGreen);
  pt_vs_a0Sig_Fi_B->SetMarkerColor(kRed);
  pt_vs_a0Sig_Fi_S->SetMarkerStyle(1);
  pt_vs_a0Sig_Fi_B->SetMarkerStyle(1);
  pt_vs_a0Sig_Fi_B->Draw();
  pt_vs_a0Sig_Fi_S->Draw("same");

  // for BDT
  c4->cd(4);
  pt_vs_a0Sig_BDT_S->SetMarkerColor(kGreen);
  pt_vs_a0Sig_BDT_B->SetMarkerColor(kRed);
  pt_vs_a0Sig_BDT_S->SetMarkerStyle(1);
  pt_vs_a0Sig_BDT_B->SetMarkerStyle(1);
  pt_vs_a0Sig_BDT_B->Draw();
  pt_vs_a0Sig_BDT_S->Draw("same");
  
  // form ANN
  c4->cd(5);
  pt_vs_a0Sig_MLP_S->SetMarkerColor(kGreen);
  pt_vs_a0Sig_MLP_B->SetMarkerColor(kRed);
  pt_vs_a0Sig_MLP_S->SetMarkerStyle(1);
  pt_vs_a0Sig_MLP_B->SetMarkerStyle(1);
  pt_vs_a0Sig_MLP_B->Draw();
  pt_vs_a0Sig_MLP_S->Draw("same");
  
  
}
