/*
 * Example of the TMVA package (Multivariate analysis)
 * for event clasification
 *
 * Example 1: traning of the Multivariate analyzers
 *      a/ rectangular cuts
 *      b/ Fisher discriminant (analytic solution, no training needed)
 *      c/ Multidimensional Probability Density Estimator (likelihood ratio)
 *      d/ Boosted decision trees
 *      e/ Artifitial neural networks (ANN)
 *
 * Execute by : root -l lesson10_example01.cpp+
 */

#include <cstdlib>
#include <iostream>
#include <map>
#include <string>

#include "TChain.h"
#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TObjString.h"
#include "TSystem.h"
#include "TROOT.h"
#include "TMath.h"

#include "TMVA/Factory.h"
#include "TMVA/Tools.h"
#include "TMVA/DataLoader.h"

// main function
void MVA_example01() {

  // This loads the TMVA library
  TMVA::Tools::Instance();
  
  // --- Here the preparation phase begins
  
  // Create a ROOT output file where TMVA will store ntuples, histograms, etc.
  TString outfileName( "TMVA.root" );
  TFile* outputFile = TFile::Open( outfileName, "RECREATE" );
  
  // Create the factory object. Later you can choose the methods. 
  // The factory is 
  // the only TMVA object you have to interact with
  TMVA::Factory *factory = new TMVA::Factory( "TMVAClassification",   // name prefix
                                              outputFile,             // output file for the MVA optimization
                                             "!V:!Silent:Color:DrawProgressBar:Transformations=I;D;P;G,D:AnalysisType=Classification" ); // settings


  TMVA::DataLoader *dataLoader = new TMVA::DataLoader("dataset");

  dataLoader->AddVariable ( "chi2"  , 'D' );  // minimized chi2 in the vertex fit
  dataLoader->AddVariable ( "lxySig", 'D' );  // decay distance significance
  dataLoader->AddVariable ( "a0Sig" , 'D' );  // impact parameter significance
  dataLoader->AddVariable ( "pt"    , 'D' );  // transverse momentum

  // You can add so-called "Spectator variables", which are not used in the MVA training
  dataLoader->AddSpectator( "mass", 'D' );    // reconstructed mass

  // read varianles from the TTree and pass them to the TMVA factory:
  TFile* input = TFile::Open( "skimThin.mc10.Lam+LamBar.MinBias.SimpleTree.01.root" );
  TTree* t     = (TTree*)input->Get("Lam");  // TTree for Lam->p+pi decays
  vector<double> vars;    // auxiliary variable

  // setup tree branches  
  vector<double> *chi2 = 0, *px = 0, *py = 0, *mass = 0;
  vector<vector<double> > *lxy = 0, *lxyError = 0, *a0 = 0, *a0Error = 0;
  vector<int> *isSignal = 0;
  t->SetBranchAddress("VTX_chi2"    , &chi2);
  t->SetBranchAddress("VTX_lxy"     , &lxy);
  t->SetBranchAddress("VTX_lxyError", &lxyError);
  t->SetBranchAddress("VTX_a0"      , &a0);
  t->SetBranchAddress("VTX_a0Error" , &a0Error);
  t->SetBranchAddress("VTX_px"      , &px);
  t->SetBranchAddress("VTX_py"      , &py);
  t->SetBranchAddress("VTX_mass"    , &mass);
  t->SetBranchAddress("isSignal"    , &isSignal);
  
  // loop over events in the input tree
  for(int i=0; i<t->GetEntries(); ++i) {
    if(i%50000==0) cout<< "event " << i << << "/" << t->GetEntries() << endl;
    t->GetEntry(i);
    
    // loop over Lam candidates in each single event
    for(uint j=0; j<chi2->size(); ++j) {
      vars.clear();
      vars.push_back( chi2->at(j) );                                                  // vertex fit min chi2
      vars.push_back( lxy->at(j)[0]/lxyError->at(j)[0] );                             // decay distance significance
      vars.push_back( a0->at(j)[0]/a0Error->at(j)[0] );                               // impact parameter significance
      vars.push_back( TMath::Sqrt(px->at(j) * px->at(j) + py->at(j) * py->at(j)));    // transverse momentum
      vars.push_back( mass->at(j));                                                   // reconstructed mass
      
      // pass the events to the TMVA dataLoader:
      if(isSignal->at(j)==1) {
        // MC events is signal:
        if (i < t->GetEntries()/2.0)
          dataLoader->AddSignalTrainingEvent( vars, 1.0 ); // half of the events is used for training
        else
          dataLoader->AddSignalTestEvent( vars, 1.0 ); // half of the events is used for testing
      }else{
        // MC event is background:
        if (i < t->GetEntries()/2.0)
          dataLoader->AddBackgroundTrainingEvent( vars, 1.0 ); // half of the events is used for training
        else
          dataLoader->AddBackgroundTestEvent( vars, 1.0 ); // half of the events is used for testing
      }
    } // end of loop over Lam candidates
  } // end of loop over events
    
  // Tell the dataLoader how to use the training and testing events
  dataLoader->PrepareTrainingAndTestTree( "", "","SplitMode=Random:!V" );
  
  // ---- Book MVA methods
  // Please lookup the various method configuration options here: http://tmva.sourceforge.net/optionRef.html
  
  //----------------------------------
  // a/ rectangular cut optimisation
  //----------------------------------
  factory->BookMethod( dataLoader, TMVA::Types::kCuts, "Cuts",
                       "!H:!V:"
                       "FitMethod=MC:"                                            //!< optimization method (MC, GA, or SA)
                       "CutRangeMin[0]=0:CutRangeMax[0]=10:VarProp[0]=FMin:"      //!< range for chi2 cut
                       "CutRangeMin[1]=0:CutRangeMax[1]=10000:VarProp[1]=FMax:"   //!< range for lxy significance cut
                       "CutRangeMin[2]=0:CutRangeMax[2]=10:VarProp[2]=FMin:"      //!< range for a0 significance cut
                       "CutRangeMin[3]=0:CutRangeMax[3]=30000:VarProp[3]=FMax:"   //!< range for pT cut
                       "EffSel:SampleSize=100000" );                              //!< other parameters

  //----------------------------------
  // b/ Fisher dicriminant
  //----------------------------------
  factory->BookMethod( dataLoader, TMVA::Types::kFisher, "Fisher", 
                       "H:!V:Fisher:"
                       "VarTransform=None:"           //!< List of variable transformations performed before training. e.g D = decorrelation
                       "CreateMVAPdfs:"               //!< Create PDFs for classifier outputs
                       "PDFInterpolMVAPdf=Spline2:"   //!< interpolation method used for the classifier output PDF parametration
                       "NbinsMVAPdf=50:"              //!< number of bins for classifier output PDF
                       "NsmoothMVAPdf=10"             //!< smoothing parameter for classifier output PDF 
                       );
  
  // //----------------------------------
  // // c/ Multidimensional Probability Density Estimator (likelihood ratio)
  // //   Not covered by the lecture :-)
  // //----------------------------------
  // // k-nearest naibourgh method
  // factory->BookMethod( dataLoader, TMVA::Types::kKNN, "KNN",
  //                      "!H:"
  //                      "nkNN=20:"           //!< number of nearest neibourghs
  //                      );

  //----------------------------------
  // d/ Boosted decision trees
  //----------------------------------
  factory->BookMethod( dataLoader, TMVA::Types::kBDT, "BDT",
                       "!H:!V:"
                       "NTrees=850:"                    //!< number of trees in the forest
                       "MinNodeSize=2.5%:"              //!< Minimum percentage of training events required in a leaf node
                       "MaxDepth=3:"                    //!< Max depth of the decision tree allowed
                       "BoostType=AdaBoost:"            //!< Boosting type for the trees in the forest (e.g. AdaBoost or Grad)
                       "AdaBoostBeta=0.5:"              //!< Learning rate for AdaBoost algorithm
                       "SeparationType=GiniIndex:"      //!< Separation criterion for node splitting
                       "nCuts=20"                       //!< Number of grid points in variable range used in finding optimal cut in node splitting
                       );

  //----------------------------------
  // e/ Artifitial neural networks (recommended ANN)
  //----------------------------------
  factory->BookMethod( dataLoader, TMVA::Types::kMLP, "MLP", 
                       "!H:!V:"
                       "EstimatorType=CE"     //!< CE = cross-entropy for classification, MSE = mean-square-error for regression
                       "NeuronType=ReLU:"     //!< neuron response function (ReLU = max(0,x))
                       "VarTransform=N:"      //!< List of variable transformations performed before training. N=normalization
                       "NCycles=600:"         //!< number of training cycles
                       "HiddenLayers=N+5:"    //!< one hidden layer with N+5 neurons (N is number of input vars.)
                       "TestRate=5:"          //!< Test for overtraining performed at each 5th epoch
                       );
  
  // ---- Now you can tell the factory to train, test, and evaluate the MVAs
  
  // Train MVAs using the set of training events
  factory->TrainAllMethods();
  
  // ---- Evaluate all MVAs using the set of test events
  factory->TestAllMethods();
  
  // ----- Evaluate and compare performance of all configured MVAs
  factory->EvaluateAllMethods();

  // Save the output
  outputFile->Close();

  std::cout << "==> Wrote root file: " << outputFile->GetName() << std::endl;

  
}
