/* Example 5: pul distribution */

#include <sstream>
#include "RooDataSet.h"
#include "RooRealVar.h"
#include "RooPolynomial.h"
#include "RooPlot.h"
#include "RooMinimizer.h"
#include "RooMinuit.h"
#include "RooFitResult.h"
#include "TFile.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TH2D.h"
#include "TLine.h"
#include "TLegend.h"
#include "TPaveText.h"

using namespace RooFit;
using namespace std;

void MLE_example05() {
  
  // define observable: RooFit class RooRealVar
  RooRealVar x("x", "x", 0, -0.95, 0.95); // value, range_min, range_max (N.B. the value doesn't matter here)
  
  // Load the TTree form the input file
  // NOTE: it is assumed that the "scatter.root" file is already created 
  //       by running lesson3_example03.cpp
  TFile* inFile = TFile::Open("scatter.root");
  TTree* t = (TTree*)inFile->Get("scatter");

  // convert data to the RooFit class RooDataSet. The imported tree must have a branch named "x"
  RooDataSet data("data", "data", t, RooArgSet(x));
  
  /////////////////////////////
  // define fit model and perform fit
  /////////////////////////////

  // Parameters: again RooRealVar classes
  RooRealVar alpha("alpha", "#alpha", 0.8, -2, 2); // initial value, range_min, range_max
  RooRealVar beta ("beta",  "#beta",  0.8, -2, 2); // fixed wrong initial value

//  RooRealVar alpha("alpha", "#alpha", 0.8, 0.55, 2); // tighter range
//  RooRealVar beta ("beta",  "#beta",  1.25, 1.25, 1.25); // fixed wrong initial value
  
  // model: using RooPolynomial class
  RooPolynomial model("model", "1+alpha*x + beta*x^2", x, RooArgSet(alpha, beta));
  
  // perform the fit:
  RooFitResult* result = model.fitTo(data, Save(true));
  
  // plot the result: using RooFit class RooPlot
  new TCanvas("c1", "ML fit", 0, 10, 800, 600);

  RooPlot* frame_final = x.frame();
  data .plotOn(frame_final, Binning(50), LineColor(kBlack), LineWidth(2), MarkerSize(1.2), XErrorSize(1.));
  model.plotOn(frame_final, LineColor(kBlue), LineWidth(3));

  frame_final->Draw();

  /////////////////////////////
  // do pseudo-experiments
  /////////////////////////////
  TH1D* h_Lmax = new TH1D("h_Lmax", "Estimated districtution of Lmax; L_{max}", 100, -650, -550);
  
  TH1D* h_alpha_pull = new TH1D("h_alpha_pull", "Pull distribution for #alpha; (#hat{#alpha}_{j} - #hat{#alpha})/#sigma_{#alpha_{j}}", 100, -5, -5);
  TH1D* h_beta_pull = new TH1D("h_beta_pull", "Pull distribution for #alpha; (#hat{#beta}_{j} - #hat{#beta})/#sigma_{#beta_{j}}", 100, -5, -5);

  int nexp = 1000;
  int nEvt = t->GetEntries(); // size of the sample 
  double alphaHat = alpha.getVal(); // estimated alpha value
  double betaHat  = beta.getVal(); // estimated beta value
  for(int i=0; i<nexp; ++i) {
    // reset parameters to the estimated values
    alpha.setVal(alphaHat);
    beta .setVal(betaHat);
    
    // generate pseudo-data using RooFit:
    RooDataSet* pseudo_data = model.generate(x, nEvt);
    
    // perform fit on pseudo-data
    RooFitResult* pseudo_result = model.fitTo(*pseudo_data, Save(true)); // Save(kTrue) means the results are stored
    
    // fill the histograms
    h_Lmax->Fill(-pseudo_result->minNll()); //minNll returns -log L_max, hence the minus
    h_alpha_pull->Fill((alpha.getVal()-alphaHat)/alpha.getError());
    h_beta_pull->Fill((beta.getVal()-betaHat)/beta.getError());
    
    // cleanup
    delete pseudo_result;
    delete pseudo_data;
  }

  // normalize histogram:
  h_Lmax->Scale(1./h_Lmax->GetEntries());
  
  // draw
  new TCanvas("c2","c2", 0, 10, 800, 600);
  h_Lmax->Draw();
  
  // calculate P-value (note: we are rounding to the whole bins. This is sufficient)
  int bin = h_Lmax->FindBin(-result->minNll());
  TH1D* h_pvalue = (TH1D*)h_Lmax->Clone("h_pvalue");
  for(int i=bin+1; i<=1001; ++i) {
    h_pvalue->SetBinContent(i, 0);
  }
  h_pvalue->SetFillColor(kYellow);
  h_pvalue->Draw("same");
  std::cout << "P-Value = " << h_pvalue->Integral() << std::endl;
  
  // draw the observed value of Lmax
  TLine* l = new TLine(-result->minNll(), 0, -result->minNll(), h_Lmax->GetMaximum());
  l->SetLineStyle(2);
  l->Draw();
  
 TCanvas* c3 = new TCanvas("c3","c3", 0, 10, 800, 600);
  h_alpha_pull->Draw();
  h_alpha_pull->SetMaximum(1.4*h_alpha_pull->GetMaximum());
  TPaveText* stats1 = new TPaveText(0.6, 0.9, 0.9, 0.75, "NDC");
  stats1->SetFillColor(kWhite);
  stringstream ss1; ss1 << "Mean = " << h_alpha_pull->GetMean() << " #pm " << h_alpha_pull->GetMeanError();
  stats1->AddText(ss1.str().c_str());
  stringstream ss2; ss2 << "RMS = " << h_alpha_pull->GetRMS() << " #pm " << h_alpha_pull->GetRMSError();
  stats1->AddText(ss2.str().c_str());
  stats1->Draw("same");
  c3->Modified();
  c3->Update();

  TCanvas* c4 = new TCanvas("c4","c4", 0, 10, 800, 600);
  h_beta_pull->Draw();
  h_beta_pull->SetMaximum(1.4*h_beta_pull->GetMaximum());
  TPaveText* stats2 = new TPaveText(0.6, 0.9, 0.9, 0.75, "NDC");
  stats2->SetFillColor(kWhite);
  stringstream ss3; ss3 << "Mean = " << h_beta_pull->GetMean() << " #pm " << h_beta_pull->GetMeanError();
  stats2->AddText(ss3.str().c_str());
  stringstream ss4; ss4 << "RMS = " << h_beta_pull->GetRMS() << " #pm " << h_beta_pull->GetRMSError();
  stats2->AddText(ss4.str().c_str());
  stats2->Draw("same");
  c4->Modified();
  c4->Update();
  
}
