# Statistical Methods in High Energy Physics (NJSF143)


# Homework

* HW1 on the Introduction part: Solve the problems described [here](homework/homework_1.pdf) and send us a scanned copy (or photo) of your solution

* HW2 on the Maximum Likelihood Method part: Go through all 10 examples [on MLE](#maximum-likelihood-method) and finish the code snipped below. You can do either ROOT or PyROOT version. Tasks you should do:

  - Fit histogram `h` with an exponential function using:

      1. the least square fit assuming all bin errors are 1

      1. the least square fit assuming all bin errors are sqrt(n_i)

      1. binned maximum likelihood with Poisson PDF

      1. implement unbinned maximum likelihood fit using RooFit.
  
  - Compare all the results by plotting the fitted functions into a single plot. (You can put the unbinned fit result in a separate plot if you do not know how to plot it together with the other functions.)

  - Try to increase the number of generated events to see if 2. and 3. converge. Make a plot of the difference of the fitted values as a function of the number of events in the histogram.

  - You should send back the completed code and the generated plots as PDF or PNG files.

    | *Framework:* | ROOT (c++) | PyROOT | 
    |-|-|-|
    | Code snippet: | [root/MLE_homework.cpp](root/MLE_homework.cpp) | [pyroot/MLE_homework.py](pyroot/MLE_homework.py) |

* HW3 on the Hypothesis Testing: Solve the problems described [here](homework/homework_3.pdf) and send us a scanned copy (or photo) of your solution as well as your code

* HW4 on the MVA: Go through all 3 examples on [MVA](#multivariate-analysis-mva-machine-learning-ml) below and then:

  - MAIN OPTION:

    - Build an ROC curve for classifiers Fisher, BDT and MLP from the TMVA examples. 

    - Use the testing sample that’s used in the examples.

    - Calculate the integral of each ROC curve and use it to compare the three methods.

  - ALTERNATIVE: 

    - Use the classifiers trained using the TensorFlow/Keras framework (MLP and deep feed-forward network with resudual connections)

    - Plot an ROC curve for these classifiers

* HW5 on the Unfolding: Find it at the bottom of this page

# Examples

 To run these examples, download this entire repository to your local machine:
 * Using git:
    ```sh
    git clone https://gitlab.cern.ch/scheiric/StatMethodsCode.git
    ```

 * Manual download:
    * In the left menu click on `repository` -> `files`
    * In the top-right panel click on the download icon: 

        ![download](download.png)

      (You can also directly follow this [link](https://gitlab.cern.ch/scheiric/StatMethodsCode/-/archive/master/StatMethodsCode-master.zip))
    * extract the downloaded archive, preserving the directory structure.
    * All examples should be run from the top folder of the extracted archive

## Maximum Likelihood Method

 * For ROOT and PyROOT examples, you will need to install [ROOT](https://root.cern.ch/) framework.
 * For TensorFlow/Keras example, you will need to install Tensorflow and other packages (see [below](#ml))

### 1. Simple example illustrating the method of maximum likelihood
 * The gaussian-distributed data are generated using MC random number generator: `TRandom3::Gaus`
 * Likelihood function is defined in the function "logL".
 * The likelihood was calculated for three cases: the real values of parameters, obviously wrong values, and values maximizing the likelihood
 * **NOTE:** The class TF1 is used to find max likelihood in this example. This is just an illustration, do not use this method in real life. Practically usable methods of fitting are shown below.

    | *Framework:* | ROOT (c++) | PyROOT | 
    |-|-|-|
    | Example code: | [root/MLE_example01.cpp](root/MLE_example01.cpp) | [pyroot/MLE_example01.py](pyroot/MLE_example01.py) |
    | Code execution: | `root -l root/MLE_example01.cpp+` | `python3 -i pyroot/MLE_example01.py` |
 
 
### 2. Examples of various methods for estimating the fitted parameter uncertainty

* Pseudo-data are drawn from the exponential probability density function
* Fit is performed again using the TF1 class. **Do not use this **method in real life!****
* Uncertainty of the fitted parameter was estimated using four methods: analytical calculation, RFC bound, graphical method MC method. 

    | *Framework:* | ROOT (c++) | PyROOT | 
    |-|-|-|
    | Example code: | [root/MLE_example02.cpp](root/MLE_example02.cpp) | [pyroot/MLE_example02.py](pyroot/MLE_example02.py) |
    | Code execution: | `root -l root/MLE_example02.cpp+` | `python3 -i pyroot/MLE_example02.py` |


### 3. Example of fitting with the two-parameter function using Minuit2 class. Illustration of the “accept/reject” MC method.

 * Pseudo-data are this time generated “by hand” using the accept/reject method
 * Likelihood function is implemented using a lambda function and passed to the Minuit2 minimizer.
 * This is the first example usable in practical situations. However, it requires a lot of work since the fitted function must be implemented by hand together with its proper normalization factor.
 * The fit is repeated many times with different pseudo-data samples. The results are displayed as a 2D distribution. This illustrates the relation between the parameter uncertainty estimated using the RFC bound technique and the uncertainty estimated using the pseudo-experiments.

    | *Framework:* | ROOT (c++) | PyROOT | 
    |-|-|-|
    | Example code: | [root/MLE_example03.cpp](root/MLE_example03.cpp) | [pyroot/MLE_example03.py](pyroot/MLE_example03.py) |
    | Code execution: | `root -l root/MLE_example03.cpp+` | `python3 -i pyroot/MLE_example03.py` |


### 4. Fitting in RooFit

 * The previous example was rewritten using the RooFit library
 * RooFit is a standard component of ROOT. It allows a comfortable definition of the fitted PDF using the C++ classes. We recommend using RooFit for the ML fits in ROOT.
 * This example also illustrates the relation between the individual parameter uncertainties and the likelihood contour.
 * This example uses pseudo-data from the previous example. **Therefore, you need to run the previous script first!**

    | *Framework:* | ROOT (c++) | PyROOT | 
    |-|-|-|
    | Example code: | [root/MLE_example04.cpp](root/MLE_example04.cpp) | [pyroot/MLE_example04.py](pyroot/MLE_example04.py) |
    | Code execution: | `root -l root/MLE_example04.cpp+` | `python3 -i pyroot/MLE_example04.py` |

### 5. Pull distributions

* Fit from the previous example is repeated many times using pseudo-data generated using RooFit (yes, RooFit can generate any pseudo-data using any PDF)
* Pull distribution is created: $p_{i}=(\theta-\theta_i)/\sigma_{\theta_i}$
where $\theta$ is the estimated parameter, $\theta_i$ is parameter estimated in the i-th pseudo-experiment and $\sigma_{\theta_i}$ is its uncertainty.
* For unbiased estimate and correctly estimated uncertainty, pull distribution should have a mean of 0 and a standard deviation of 1. This is a standard way of checking the bias and stability of the ML fit (remember that ML can be in principle biased. Also because the minimization is done numerically, it can suffer from non-convergence, instabilities, etc.)

    | *Framework:* | ROOT (c++) | PyROOT | 
    |-|-|-|
    | Example code: | [root/MLE_example05.cpp](root/MLE_example05.cpp) | [pyroot/MLE_example05.py](pyroot/MLE_example05.py) |
    | Code execution: | `root -l root/MLE_example05.cpp+` | `python3 -i pyroot/MLE_example05.py` |

### 6. Relation between the likelihood contour and the parameter uncertainty

* Again, the ML fit using data from example 3. The likelihood contour is drawn around the estimated parameter values.
* The fit is repeated many times using pseudo-data drawn from the PDF with the true values of parameters. The results of these pseudo-experiments are filled in the 2D histogram. This histogram is visualized using a single contour (option TH2F::Draw(“cont1”)).
* This contour is roughly elliptical and has roughly the same area as the ML contour drawn previously. However, it is centered around the true value rather than around the estimate.
* Both contours are overlapping but are shifted with respect to each other. The ML contour can be interpreted as an uncertainty region **covering** the true value of the parameter with a certain probability (c.f. Neyman method).
* NOTE: The code runs for a long time. You can speed it up by reducing the number of pseudo-experiments, but then the pseudo-result contour will not be very nicely rendered.

    | *Framework:* | ROOT (c++) | PyROOT | 
    |-|-|-|
    | Example code: | [root/MLE_example06.cpp](root/MLE_example06.cpp) | [pyroot/MLE_example06.py](pyroot/MLE_example06.py) |
    | Code execution: | `root -l root/MLE_example06.cpp+` | `python3 -i pyroot/MLE_example06.py` |


### 7. Binned maximum likelihood method

 * Comparison of different methods of fitting the histogram: least-square fit, binned ML fit and unbinned ML fit.

    | *Framework:* | ROOT (c++) | PyROOT | 
    |-|-|-|
    | Example code: | [root/MLE_example07.cpp](root/MLE_example07.cpp) | [pyroot/MLE_example05.py](pyroot/MLE_example07.py) |
    | Code execution: | `root -l root/MLE_example07.cpp+` | `python3 -i pyroot/MLE_example07.py` |

### 8. Defining custom PDFs in RooFit
 * Examples of construction of a custom PDF using RooFit classes.

    | *Framework:* | ROOT (c++) | PyROOT | 
    |-|-|-|
    | Example code: | [root/MLE_example08.cpp](root/MLE_example08.cpp) | [pyroot/MLE_example08.py](pyroot/MLE_example08.py) |
    | Code execution: | `root -l root/MLE_example08.cpp+` | `python3 -i pyroot/MLE_example08.py` |

### 9. Extended ML fit
 * Comparison of the extended ML fit and ML fit for a two-component PDF.
 * The number of signal and background events extracted from the classic ML fit is calculated and compared to the results of the extended ML fit.
 * While the numbers themselves agree, uncertainties estimated in the classic ML fit do not reflect the uncertainty of the total number of events, which one has to take into account when e.g. measuring cross-section.

     | *Framework:* | ROOT (c++) | PyROOT | 
    |-|-|-|
    | Example code: | [root/MLE_example09.cpp](root/MLE_example09.cpp) | [pyroot/MLE_example09.py](pyroot/MLE_example09.py) |
    | Code execution: | `root -l root/MLE_example09.cpp+` | `python3 -i pyroot/MLE_example09.py` |

### 10. Constrained ML fit
 * Example of definition of the constrained ML fit.

    | *Framework:* | ROOT (c++) | PyROOT | 
    |-|-|-|
    | Example code: | [root/MLE_example10.cpp](root/MLE_example10.cpp) | [pyroot/MLE_example10.py](pyroot/MLE_example10.py) |
    | Code execution: | `root -l root/MLE_example10.cpp+` | `python3 -i pyroot/MLE_example10.py` |

### 11. Template fits
 * Examples of template fits are given in the Hypothesis testing section.

## Hypothesis testing

* You will need [ROOT](https://root.cern.ch/) for `HistFitter` examples
* You will need python3 and cmake for `HistFitter` examples
* You will need `HistFitter` downloaded GitHub:
    * Installation: 
        ```sh
        git clone https://github.com/histfitter/histfitter.git HistFitter_src
        cd HistFitter_src
        git checkout v1.3.0 -b v1.3.0
        mkdir build
        cd build
        cmake -DCMAKE_INSTALL_PREFIX=../../HistFitter/ ..
        make install
        cd ../..
        ```
    * Every time you want to work with HistFitter, you must point the python to its path. Run the following command from the `StatMethodsCode` directory whence you installed the HistFitter:
        ```sh
        source HistFitter/bin/setup_histfitter.sh
        ```

### Example measurement

* We are trying to measure the signal strength of a hypothetical particle called Leptoquark (LQ) with a mass of 0.5 TeV
* We have already processed our data and created histograms of the sensitive varible $S_T$ (scalar sum of the transverse moment of leading two leptons and jets in the event)
* The histograms are available for data, signal MC simulation and also all assumed background processes.
* The histograms are stored in the file `data/wsi_ST_allsyst.root`
* The histograms for the signal region (SR) are stored in the ROOT file folder called `chan_hh__cat_presel__sr`. There are also several control regions available, such as `chan_hh__cat_presel__cr_ztt` and `chan_1tau__cat_presel__cr_top2cr`.
* The histograms are further divided into sub-folders called `LQ500`, `Ztt`, `ttbar`, etc. which correspond to individual processes.
* Histograms that correspond to our "best-guess" prediction for each process are called `nominal`.
* Appart from the `nominal` histograms, we also have many histograms that cover the systematic uncertainties on theory predictions as well as experimental uncertainties.

### 1. Stat-only template fit in the SR

* Simple example showing how to perform the template fit in the SR.
* We only consider statistical uncertainties of the MC simulation, but we do not have any other systematic uncertainties.
* We have three normalization factors in the fit: 
    * 'mu' - signal strength
    * 'NF_Z' - normalization factor for the Z boson background
    * 'NF_t` - normalization factor for the top quark background

* Running the example:

    | *Framework:* | HistFitter (PyROOT) | PyHF |
    |-|-|-|
    | Example code: | [pyroot/HT_example01.py](pyroot/HT_example01.py) | TBD |
    | Code execution: | `HistFitter.py -w -f -F "bkg" -m "all" -D "before,after,corrMatrix" pyroot/HT_example01.py` | TBD |
    | Results: | $S_T$ plot before fit: `results/TheFit01/sr_S_T_beforeFit.pdf`<br>$S_T$ plot after fit: `results/TheFit01/sr_S_T_afterFit.pdf`<br>Correlation matrix: `results/TheFit01/c_corrMatrix_RooExpandedFitResult_afterFit.pdf` | TBD |

* Let's have a look at the results:
    * The "beforeFit" plots shows our model before the template fit as it compares to data. In the bottom pannel, you can see the ratio "Data/Backgroun-only model" (red squares) and the ratio "Data/Signal+Background model" (black circles). You can see that our prefit model does not describe the data very well, namely the predicted signal yield is unreasonably high.
    * The "afterFit" plots shows our model after the template fit. You can see that the model describes the data much better now. The signal contribution is greatly reduced with mu reduced to about $0.027 \pm 0.015$. However, the data are still not modeled perfectly, because we do not consider any systematic uncertainties. We will add them in the next.
    * The correlation matrix shows that we have huge correlations (up to 90%) between the signal strength and normalization factors. This is not a good situation, because it means that our fit is not able to distinguish between the signal and background contributions. This is caused by the fact that we do not have any control regions that would help us to constrain the background contributions. We will add them in the next example.
    * The best fit values for the "mu", "NF_Z", "NF_t" and other (nuisance) parameters are stored printed in the terminal output. You can also find them in the file `results/TheFit01/TheFit01_combined_NormalMeasurement_model_afterFit.root` file.
    * NOTE: the graphical output of the HistFitter is not very nice. It is usable for the analysis development, but for the final plots, you will have use your own plotting scripts. Fortunatelly, all the plots are outputted as ROOT files, so you can easily read them and plot them using your own scripts. Alternatively, you can use `pyhf` and matplotlib (examples to follow).

### 2. Template fit with CRs and systematic uncertainties

* We use the previous example and add CRs for Z boson and top quark backgrounds, as well as some few systematic uncertainties.

* Running the example:

    | *Framework:* | HistFitter (PyROOT) | PyHF |
    |-|-|-|
    | Example code: | [pyroot/HT_example02.py](pyroot/HT_example02.py) | TBD |
    | Code execution: | `HistFitter.py -w -f -F "bkg" -m "all" -D "before,after,corrMatrix" pyroot/HT_example02.py` | TBD |
    | Results: | $S_T$ plot before fit: `results/TheFit02/sr_S_T_beforeFit.pdf`<br>$S_T$ plot after fit: `results/TheFit02/sr_S_T_afterFit.pdf`<br>Correlation matrix: `results/TheFit02/c_corrMatrix_RooExpandedFitResult_afterFit.pdf` | TBD |

* Let's have a look at the results:
    * We now have plots also for the control regions
    * The "beforeFit" plots are equally bad as the in the previous example.
    * However, the "afterFit" plots now show much better agreement between the data and the model. The signal strength is now reduced to $-0.005 \pm 0.005$, in agreement with 0. This means we have not observed any signal in our data. Data can be well described by the background-only model.
    * The correlation matrix is now much healthier, too. The correlations between the signal strength and normalization factors are now much smaller. This is because we have now control regions that help us to constrain the background contributions.

### 3. setting the limit on the signal strength

* We use the previous example and perform the hypotesis test inversion to set a limit on the signal strength $\mu$.

* Running the example:

    | *Framework:* | HistFitter (PyROOT) | PyHF |
    |-|-|-|
    | Code execution: | `HistFitter.py -w -f -F "excl" -p -l pyroot/HT_example02.py` | TBD |
    | Results: | Limit plot: `results/upperlimit_cls_poi_LQ500_Asym_CLs_grid_ts3.root.pdf` | TBD |

* Let's have a look at the results:
    * The upper limit plots shows the p-value as a function of the assumed signal strength.
    * If the p-value drops below 0.05 we reject the signal+background hypothesis at 95% confidence level.
    * Setting an upper limit on our signal model strength means that we are looking for the mu value that has the p-value exactly 0.05. Below this mu value, we cannot reject our hypothesis so it means our model could still exist.
    * There are several curces in the plot. The relevant one is called "Observed CLs". This is the curve that we actually use for the limit setting (CLb and CLs+b are components of the CLs method).
    * Another interesting curve is the "Expected CLs - Median". This is the median expected limit estimated using our MC prediction (so called Asimov data). The "Expected CLs - 1 sigma" and "Expected CLs - 2 sigma" curves show the 1 and 2 sigma bands around the median expected limit.
    * From the plot we can see that our limit is somewhere at $\mu = 0.0085$, lower than the expected limit of $\mu = 0.0115$. 
    * However, the observed limit still falls within the $2\sigma$ band of the expected limit. 

### 4. discovery test

* From the previous examples it is clear that we have not observed any signal in our data. 
* However, we can still perform a discovery test. In this test, we will measure the p-value of the background-only hypothesis. 
* If this value came out small (smaller that $3\times 10^{-8}$), it would mean that background model is not able to describe the data and we would have to reject the background-only hypothesis.
* However, since we already know the background describes our data well, we expect the p-value to be large.

    | *Framework:* | HistFitter (PyROOT) | PyHF |
    |-|-|-|
    | Code execution: | `HistFitter.py -w -f -F "bkg" -z pyroot/HT_example02.py` | TBD |
    | Results: | p-value and significance printed on the screen | TBD |

* Let's have a look at the results:
    - Null p-value = 0.318809
    - Significance = 0.471032
    * The p-value is large, as expected. We cannot reject the background-only hypothesis.
    * The significance (which is just a reinterpretation of the p-value) is small, as expected. We cannot claim a discovery of the signal. Threshold for the discovery is 5 sigma, so we would need a significance of 5 to claim a discovery.


## Multivariate analysis (MVA), machine learning (ML)

 * For ROOT and PyROOT examples, you will need to install [ROOT](https://root.cern.ch/) framework.
 * <a name="ml"></a>For TensorFlow/Keras examples you will need to set the virtual environment as follows:
    ```sh
    python3 -m venv venv
    source venv/bin/activate
    pip install awkward
    pip install pandas    
    pip install awkward-pandas
    pip install uproot
    pip install tensorflow
    pip install matplotlib
    ```
 
 * NOTE: You should have at least python3.8. In older versions, some of the packages will not work.

### 1. Comparison of methods “Orthogonal cuts”, “Fisher discriminant”, “BDT” and “ANN” with the use of the TMVA package and TensorFlow/Keras package

 * TMVA Libraries offer a unified interface for the usage of several MVA methods
 * The example is based on a simulation of Lambda hyperon decays to a proton and a pion
 * The following discriminating variables are used:
    - Chi2_min: a measure of the vertex reconstruction quality. The smaller the value, the better the vertex.
    - Transverse momentum
    - Transverse decay length significance – a transverse distance between the decay vertex and the primary vertex, divided by the uncertainty.
    - Transverse impact parameter significance – a transverse distance of the Lambda hyperon trajectory from the primary vertex, divided by the uncertainty.
 * The MC sample is split into two halves: training and testing samples
 * Four selection methods are trained with the use of the training sample:
    - Cuts: application of 4 selection criteria on the discriminating variables
    - Fisher: Fisher Discriminant
    - BDT: Boosted Decision Tree
    - MLP: Artificial Neural Network Multilayer Perceptron
 * Each method has some parameters, so-called hyperparameters, that need to be set by the user. In the example, they are set to some “reasonable” default
 * The training results are saved to an output ROOT file. Try to open the file in the ROOT’s TBrowser and look at the saved histograms
 * Optimized values of all parameters of each method are saved in an XML file in the “dataset/weights/” folder. You will use it later for your work
 * TMVA compares the different methods with the use of the ROC integral. Note the section “Evaluation results ranked by best signal efficiency and purity (area)” in the TMVA printout on the screen
 * TMVA also performs an overtraining test. Signal efficiencies are compared for the training and the testing samples for three values of the background rejection. A much larger significance for the training sample is a clear sign of the classifier overtraining.

 * Multilayer perceptron (MLP) is also implemented using the TensorFlow/Keras framework by Google. Unlike TMVA, this is a modern framework used by machine-learning applications outside of HEP. TensorFlow/Keras is not coupled to ROOT, therefore we use the `uproot` package to convert the ROOT files into a format usable with TensorFlow.
 * An additional example shows how a deep NN (deep feed-forward netwokr with residual connections) can be built using the TensorFlow/Keras framework.
 * **NOTE**: At first glance, using the TensorFlow/Keras framework is more complicated because one needs to convert data and implement the network "by hand". However, unlike with TMVA, with TensorFlow/Keras one can build modern network topologies and use GPU to accelerate the training. It is also one of the two most used ML frameworks, so one can learn from experience gained in other fields.
 
     | *Framework:* | TMVA in ROOT (c++) | TMVA in PyROOT | TensorFlow/Keras |
    |-|-|-|-|
    | Example code: | [root/MVA_example01.cpp](root/MVA_example01.cpp) | [pyroot/MVA_example01.py](pyroot/MVA_example01.py) | [python/ML_example01.py](python/ML_example01.py)<br>[python/ML_example01.bonus.py](python/ML_example01.bonus.py)
    | Code execution: | `root -l root/MVA_example01.cpp+` | `python3 pyroot/MVA_example01.py` | `source venv/bin/activate`<br>`python3 python/ML_example01.py`<br>`python3 python/ML_example01.bonus.py` |

### 2. MVA classifiers and the testing sample
* We will read the optimized weights for three methods from the previous step (Fisher, BDT, MLP) and we will draw the test statistic distributions estimated with the testing sample.
* The separation of signal and background is clear in the displayed histograms

     | *Framework:* | TMVA in ROOT (c++) | TMVA in PyROOT | TensorFlow/Keras |
    |-|-|-|-|
    | Example code: | [root/MVA_example02.cpp](root/MVA_example02.cpp) | [pyroot/MVA_example02.py](pyroot/MVA_example02.py) | [python/ML_example02.py](python/ML_example02.py)
    | Code execution: | `root -l root/MVA_example02.cpp+` | `python3 -i pyroot/MVA_example02.py` | `python3 python/ML_example02.py` |


### 3. MVA application to the data
* We will apply the classifiers from Example 1 to the data
* Cut values are set “by eye” such that all methods have similar background rejections
* The histogram “Mass of selected events” displays the Lambda invariant mass spectrum selected by 4 methods
* The amount of signal is different for the different methods. The best performance is achieved by the MLP and BDT. Cuts have the lowest signal efficiency.
* The panel named "MVA selection regions" displays a region selected by each method
* The TensorFlow/Keras example only compares the mass distributions after event selection with the two NN classifiers trained in Example 1.

     | *Framework:* | TMVA in ROOT (c++) | TMVA in PyROOT | TensorFlow/Keras |
    |-|-|-|-|
    | Example code: | [root/MVA_example03.cpp](root/MVA_example03.cpp) | [pyroot/MVA_example03.py](pyroot/MVA_example03.py) | [python/ML_example03.py](python/ML_example03.py)
    | Code execution: | `root -l root/MVA_example03.cpp+` | `python3 -i pyroot/MVA_example03.py` | `python3 python/ML_example03.py` |

## Unfolding

* You will need ROOT for your work with the [RooUnfold](https://gitlab.cern.ch/RooUnfold/RooUnfold) package.
* Download [RooUnfold](https://gitlab.cern.ch/RooUnfold/RooUnfold) and compile it according to the instructions in the section Building the Library on the GitLab. You can also just install the Library with pip.
    ```sh
    git clone https://gitlab.cern.ch/RooUnfold/RooUnfold.git
    cd RooUnfold
    mkdir build
    cd build
    cmake ..
    make -j4
    cd ..
    source build/setup.sh
    ```
* Run RooUnfoldExample.py:
    ```sh
    python -i examples/RooUnfoldExample.py
    ```
* Read the example
  * It uses Gaus(x; 0, 2) as a truth spectrum.
  * Detector effects are simulated with a smear function that simulates a finite detector efficiency and resolution as well as a shift of the measured values wrt. the true ones.
  * The example shows how to create the response matrix. It uses BreitWigner(x; 0.3, 2.5) as a truth spectrum used to build the matrix
  * Finally, the measured spectrum is unfolded with a method of the user’s choice. The default is RooUnfoldBayes which is D’Agostini’s method.



### Homework on Unfolding
* Take the example code examples/RooUnfoldExample.py and compare the results (unfolded spectra) of three different methods:
  * RooUnfoldBayes (D’Agostini’s method)
  * RooUnfoldSvd (SVD)
  * RooUnfoldIds (IDS – Iterative Dynamically stabilized method of data unfolding)
  * Your output should be a plot showing the three spectra
* Use pseudo experiments to confirm that D’Agostini’s method estimates statistical uncertainties on the unfolded spectrum correctly:
  * Again, take `examples/RooUnfoldExample.py`
  * Print out statistical uncertainties of the unfolded spectrum estimated with D’Agostini’s method. Something like:
    ```python
    for i in range(1, hReco.GetNbinsX()+1): 
        print(hReco.GetBinError(i))
    ```
  * Estimate the statistical uncertainties with the use of pseudo experiments:
    * In each pseudo experiment:
        * Prepare the “measured” spectrum such that event counts in the individual bins are random numbers drawn from a Poisson distribution: 
            ```python
            gRandom.Poisson(hMeas.GetBinContent(i))
            ```
        * Unfold the “measured” spectrum
    * Estimate the covariance matrix for the unfolded spectrum bins:
        * Evaluate the covariance as:
            ```
            cov[bin i, bin j] = 1/nToy ∑_k (unf_k.GetBinContent(i) – mean[unf_bin_i]) * (unf_k.GetBinContent(j) – mean[unf_bin_j])
            ```
        * `unf_k` is the unfolded spectrum from the k-th pseudo experiment, `mean[unf_bin_i]`` is the mean value of a bin content (averaged over pseudo experiments)
        * `nToy` is the number of pseudoexperiments. Choose 100
        * Compare the square root of `cov[bin i, bin i]` with the uncertainties estimated by D’Agostini’s method


    



  





