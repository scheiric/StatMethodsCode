""" Example 2: uncertainty of the ML estimator
"""
from ROOT import TH1D, TH2D, TRandom3, TSystem, TCanvas, TStyle, TF1, TLine, TLegend, gStyle
from ROOT import kBlue, kWhite, kRed
import statistics
import math

## set stat-box style
gStyle.SetOptStat(1101)

## Input parameters
n = 50
tau = 1.062
print("Input value of tau: ", tau)

## Generate data (exponential pdf)
hdata = TH1D("hdata", "hdata; t; Events", 10, 0, 10)
hdata.SetStats(0)
rnd = TRandom3 (1266)
events = []
for i in range(n):
  x = rnd.Exp(tau)
  events.append( x )
  hdata.Fill( x )

## Calculate sample mean
sample_mean = statistics.mean(events)
print("Sample mean: ", sample_mean) 

## define the liklihood function
def logL(xx, _):
  tau = xx[0]
  logL = sum(math.log(1/tau) - x/tau for x in events)
  return logL

## Pass the likelihood function to the TF1 
fLogL = TF1("logL", logL, 0.1, 1.9, 0)
fLogL.SetTitle(";t;log L")

## a very simple way to maximize likehlihood (do not use in the real life!)
tau_measured = fLogL.GetMaximumX()
print("ML estimate of tau: ",tau_measured)

c1 = TCanvas("c1", "Data and the ML fit")
hdata.Draw()

## display exponential function with the ML fitted tau:
fExp = TF1("fExp", "1./[0]*TMath::Exp(-x/[0])*[1]", 0, 10)
fExp.SetLineColor(kBlue)
fExp.SetParameter(0, tau_measured); # set tau to the ML extimator
fExp.SetParameter(1, hdata.Integral()); # normalize to the histogram area
fExp.Draw("same")

## legend
legend = TLegend(0.5, 0.7, 0.85, 0.85)
legend.SetTextFont(42)
legend.SetTextSize(0.05)
legend.SetFillColor(kWhite)
legend.AddEntry(hdata, "Data", "l")
legend.AddEntry(fExp , "ML fit", "l")
legend.Draw("same")

## plot the likelihood function onto a canvas
c2 = TCanvas("c2", "logL function")
axs = TH2D("axs",";t;log L", 100, 0.8, 1.8, 100, -61, -54)
axs.SetStats(0)
axs.Draw()
ls = []
for i in range(100):
  l = TLine(0.8+i*0.01, fLogL.Eval(0.8+i*0.01), 0.8+(i+1)*0.01, fLogL.Eval(0.8+(i+1)*0.01))
  l.SetLineColor(kRed)
  l.SetLineWidth(3)
  l.Draw()
  ls.append(l)

######################
## calculate variance analytically: sigma^2 = tau_measured^2 / n
######################
print("Analytically estimated sigma[mean_tau]: ", tau_measured/math.sqrt(n))

######################
## calculate variance using RCF bound: second derivative of logL at mean = mean_measured
######################
d2LogL_over_dTau2 = fLogL.Derivative2(tau_measured)
print("Sigma[mean_tau] from RCF bound: ", 1./math.sqrt(-d2LogL_over_dTau2)); 

######################
## calculate variance using graphical method: solve logL(tau +/- sigmaTau) = logL_max - 1/2
######################
logL_max = fLogL.Eval(tau_measured)

## find the lower bound
tau_minus = fLogL.GetX(logL_max-0.5, 0.8, tau_measured); # find the solution in range 0.8 .. tau_measured

## find the upper bound
tau_plus = fLogL.GetX(logL_max-0.5, tau_measured, 1.8); # find the solution in range tau_measured .. 1.8
print("Sigma[mean_tau] from the graphical method: ",
  " sigma- = ", tau_measured-tau_minus,
  ", sigma+ = ", tau_plus-tau_measured,
  ", average = ", (tau_plus-tau_minus)*0.5); 

## draw lines
l1 = TLine(tau_minus, logL_max-0.5, tau_plus, logL_max-0.5)
l1.SetLineStyle(2)
l1.Draw()

l2 = TLine(tau_minus, logL_max-0.5, tau_minus, -61)
l2.SetLineStyle(2)
l2.Draw()

l3 = TLine(tau_plus, logL_max-0.5, tau_plus, -61)
l3.SetLineStyle(2)
l3.Draw()

l4 = TLine(tau_measured, logL_max, tau_measured, -61)
l4.SetLineStyle(2)
l4.Draw()


######################
## pseudo-expriments
######################
nexp = 10000

h_exper = TH1D("h_exper", "ML estimates #hat{#tau} for 10000 experiments with 50 observations;#hat{#tau};Pseudo-experiments", 100, 0, 2)
h_exper.SetStats(0)
for _ in range(nexp):
  events.clear()
  for i in range(n):
      x = rnd.Exp(tau_measured)
      events.append( x )

  h_exper.Fill( fLogL.GetMaximumX() )
  
c3 = TCanvas("c3", "pseudo experiments");
h_exper.Draw()

## calculate variance from pseudoexperiments:
print("sigma[mean_tau] from pseudo experiments: ", h_exper.GetRMS())

## save the canvases
c1.Print("MLE_example02.c1.pdf")
c2.Print("MLE_example02.c2.pdf")
c2.Print("MLE_example02.c3.pdf")



