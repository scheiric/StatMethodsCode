"""
 Example of the TMVA package (Multivariate analysis)
 for event clasification

Example 2: application of the trained MVA clasifiers on testing MC sample

 Make sure you run the MVA_example01.py beforehand

"""

 
from ROOT import TChain, TFile, TString, TTree, TCanvas, TString, TObjString, TSystem, TROOT, TMath, TLegend, TH2F, TLine, TMVA, TH1F, kGreen, kRed
import math
from array import array

## This loads the library
TMVA.Tools.Instance()

## --- Create the Reader object
reader = TMVA.Reader( "!Color:!Silent" )    

## Create a set of variables and declare them to the reader
## - the variable names MUST corresponds in name and type to those given in the weight file(s) used
fChi2  = array('f', [0])
lxySig = array('f', [0])
a0Sig  = array('f', [0])
pt     = array('f', [0])
reader.AddVariable( "chi2",   fChi2 )
reader.AddVariable( "lxySig", lxySig)
reader.AddVariable( "a0Sig",  a0Sig )
reader.AddVariable( "pt",     pt    )

## Spectator variables declared in the training have to be added to the reader, too
mass = array('f', [0])
reader.AddSpectator( "mass", mass)

## --- Book the MVA methods
reader.BookMVA( "Cuts method", "dataset/weights/TMVAClassification_Cuts.weights.xml" ) 
reader.BookMVA( "Fisher method", "dataset/weights/TMVAClassification_Fisher.weights.xml" ) 
reader.BookMVA( "BDT method", "dataset/weights/TMVAClassification_BDT.weights.xml" ) 
reader.BookMVA( "MLP method", "dataset/weights/TMVAClassification_MLP.weights.xml" ) 

## Book output histograms
nbin = 100

## MVA output
histFi_S  = TH1F( "MVA_Fisher_S",        "MVA_Fisher",        nbin, -1, 1 )
histBdt_S = TH1F( "MVA_BDT_S",           "MVA_BDT",           nbin, -1, 0.4 )
histNn_S  = TH1F( "MVA_MLP_S",           "MVA_MLP",           nbin, 0, 1 )
histFi_B  = TH1F( "MVA_Fisher_B",        "MVA_Fisher",        nbin, -1, 1 )
histBdt_B = TH1F( "MVA_BDT_B",           "MVA_BDT",           nbin, -1, 0.4 )
histNn_B  = TH1F( "MVA_MLP_B",           "MVA_MLP",           nbin, 0, 1 )

## read DATA file
input = TFile.Open( "data/skimThin.mc10.Lam+LamBar.MinBias.SimpleTree.01.root" )
t     = input.Get("Lam")  ## TTree for Lam.p+pi decays

## loop over events in the input tree
for i in range(t.GetEntries()//2, t.GetEntries()):
  if i%50000==0: print("event " , i , "/", t.GetEntries())
  
  ## read event    
  t.GetEntry(i)
  
  ## loop over Lam candidates in each single event
  for j in range(t.VTX_chi2.size()):

    ## fill the TMVA variables
    fChi2[0]  =  t.VTX_chi2.at(j)                                      ## vertex fit min chi2
    lxySig[0] =  t.VTX_lxy.at(j)[0]/t.VTX_lxyError.at(j)[0]            ## decay distance significance
    a0Sig[0]  =  t.VTX_a0.at(j)[0]/t.VTX_a0Error.at(j)[0]              ## impact parameter significance
    pt[0]     =  math.sqrt(t.VTX_px.at(j) ** 2 + t.VTX_py.at(j) ** 2)  ## transverse momentum
    mass[0]   = t.VTX_mass.at(j)                                      ## reconstructed mass
    
    ## --- Return the MVA outputs and fill into histograms
    if t.isSignal.at(j):
      ## event is signal
      histFi_S   .Fill( reader.EvaluateMVA( "Fisher method") )
      histBdt_S  .Fill( reader.EvaluateMVA( "BDT method"   ) )
      histNn_S   .Fill( reader.EvaluateMVA( "MLP method"   ) )
    else:
      ## event is background
      histFi_B   .Fill( reader.EvaluateMVA( "Fisher method") )
      histBdt_B  .Fill( reader.EvaluateMVA( "BDT method"   ) )
      histNn_B   .Fill( reader.EvaluateMVA( "MLP method"   ) )

## display score distributions
c1 =  TCanvas("c1", "Fisher discriminant", 200, 200, 800, 600)
histFi_S.SetLineColor(kGreen)
histFi_B.SetLineColor(kRed)
histFi_S.Draw()
histFi_B.Draw("same")

c2 = TCanvas("c2", "BDT", 200, 200, 800, 600)
histBdt_S.SetLineColor(kGreen)
histBdt_B.SetLineColor(kRed)
histBdt_S.Draw()
histBdt_B.Draw("same")

c3 = TCanvas("c3", "MLP", 200, 200, 800, 600)
histNn_S.SetLineColor(kGreen)
histNn_B.SetLineColor(kRed)
histNn_S.Draw()
histNn_B.Draw("same")

