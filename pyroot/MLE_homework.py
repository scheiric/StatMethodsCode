"""
Homework: histogram fits with different models

Run: python3 -i MLE_homework.cpp
"""

from ROOT import TH2D, TRandom3, TSystem, TCanvas, TStyle, TF1, TLine, TMath, TH1D


####################################################################/
## generate data
####################################################################/

n = 50
tau = 1.062

hdata = TH1D("hdata", "hdata t events", 10, 0, 10)
rnd = TRandom3(1266)
for i in range(n):
   x = rnd.Exp(tau)
   hdata.Fill( x )


####################################################################/
## DEFINE FITTED FUNCTION
####################################################################/
fun = TF1("fun", "[1]*TMath::Exp(-x/[0])", 0, 10)
## set starting point
fun.SetParameter(0, 0.8) ## mean lifetime. We "don't know" the value so just guess
fun.FixParameter(1, n*10./hdata.GetNbinsX())   ## normalization factor. Will be fixed

####################################################################/
## HERE PUT YOUR SOLUTION
####################################################################/
## example solution for a)
hdata.Fit(fun, "W")





