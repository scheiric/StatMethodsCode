""" Example 1: likelihood function and ML fit
"""

from ROOT import TF1, TH1D, TRandom3, TCanvas, TLegend
from ROOT import kBlack, kBlue, kWhite
import math

## define PDF: normalized gaussian pre-defined in ROOT "gausn(0)"
f1 = TF1("f1", "gausn(0)", 3, 17)
f1.SetTitle(";x;Probability")
f1.SetParameter(0, 1)  # 1st parameter: constant -- must be 1 to have the gaussian normalized!
f1.SetParameter(1, 10) # 2nd parameter: mean
f1.SetParameter(2, 1)  # 3rd parameter: sigma

## Define another PDF with different (wrong) parameters
f2 = TF1("f2", "gausn(0)", 3, 17)
f2.SetLineColor(kBlue)
f2.SetParameters(1, 7, 1) # constant, mean, sigma

## Define  PDF we will fit using the maximum likelihood method
f3 = TF1("f3", "gausn(0)", 3, 17)
f3.SetLineColor(kBlack)
f3.SetParameters(1, 10, 1)  # constant, mean, sigma

## Generate gaussian data:
true_mean  = 10 # generated distribution mean
true_sigma = 1  # generated distribution sigma
n          = 50 # no. of events

rnd = TRandom3 (1266) # random generator
hdata = TH1D("hdata", "Datax", 10000, 2, 17)  # TH1 class to display data
events = [] # list to store generated events
for i in range(n):
	x = rnd.Gaus(true_mean, true_sigma ) # generate normal distributed values
	hdata.Fill(x, 0.1) # store data 
	events.append(x)   # store data for the logL function (see below)

## define the log likelihood function
def logL(xx, _):
  mean  = xx[0]
  sigma = 1     # for the sake of simplicity we fix the sigma to the correct value

	## sum up the logarithms
  logL = sum( math.log(1./math.sqrt(2*math.pi)) + 
              0.5*math.log(1./sigma/sigma) - 
              0.5*(x-mean)*(x-mean)/sigma/sigma for x in events)

  return logL

## pass it to the TF1:	
fLogL = TF1("fLogL", logL, 3, 17, 0)

## calculate likelihood:
logL1 = fLogL.Eval(f1.GetParameter(1)) # evaluate at the mean of the 1st PDF
logL2 = fLogL.Eval(f2.GetParameter(1)) # evaluate at the mean of the 2nd PDF

# For the 3rd PDF let's perform the ML fit.
# a very simple way to maximize likehlihood:
mean_hat = fLogL.GetMaximumX()
logL3 = fLogL.Eval(mean_hat) # evaluate the logL_max
f3.SetParameter(1, mean_hat) # pass the mean_hat to the fitted function
print("Mean_hat = ", mean_hat)

## display PDF and events:
c1 = TCanvas("c1", "Random numbers")

## display PDF
f1.Draw()
f2.Draw("same")
f3.Draw("same")

## display data
hdata.Draw("same")

## Print legend
legend = TLegend(0.6, 0.5, 0.88, 0.88)
legend.SetBorderSize(0)
legend.AddEntry(f1, "logL = {:.2f}".format(logL1), "l")
legend.AddEntry(f2, "logL = {:.2f}".format(logL2), "l")
legend.AddEntry(f3, "logL = {:.2f}".format(logL3), "l")
legend.SetFillColor(kWhite)
legend.Draw("same")

## update canvas
c1.Modified()
c1.Update()

## store into the iutput file
c1.Print("MLE_example01.pdf")


