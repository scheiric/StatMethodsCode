"""
example of RooFit model definition
RooFit documentation: ftp:##root.cern.ch/root/doc/RooFit_Users_Manual_2.91-33.pdf
Reference guide: http:##root.cern.ch/root/html534/ClassIndex.html
"""

from ROOT import RooDataSet, RooRealVar, RooGaussian, RooPolynomial, RooPoisson, RooExponential, RooAddPdf, RooPlot, RooProdPdf, TFile, TTree, TCanvas, TH1D, RooArgSet
from ROOT import RooFit, kBlue, kYellow, kWhite, kBlack, kRed, kMagenta, kCyan, kBlue, kGreen, kDashed


## STEP 1: define observables, using RooRealVar class
##         RooRealVar(name, title, value, min, max, unit)
## Parameters: value - current value of the variable. Can be anything in case of observables
##             min   - minimal allowed value
##             max   - maximal allowed value
##             unit  - optional. Unit to be displayed in the plots

## Some examples:
mass = RooRealVar("mass", "m", 0, 100, 150, "GeV")
x    = RooRealVar("x", "x", 0, -10, 150)

##---------------------------------  
## STEP 2: define model parameters. Again, class RooRealVar is used.

## examples:
mean  = RooRealVar("mean", "#mu", 60) ## if no range is given, the value is fixed
sigma = RooRealVar("sigma", "#sigma", 10, 1., 10)
fSig  = RooRealVar("fSig", "f_{sig}", 0.5, 0, 1)
slope = RooRealVar("slope", "a_{1}", -0.001)
a2    = RooRealVar("a2", "a_{2}", 1.)
c     = RooRealVar("c", "c", -0.1, -1e100, 0.) ## only allow negative values

## NOTE: parameter ranges are used when fitting the PDF to data


##---------------------------------  
## STEP 3: define your PDF.
##         RooFit has many basic PDF's pre-defined and also allows for definition of new PDF's
##         All PDF's are derived from class RooAbsPdf (abstract PDF). 

## some examples:
## (a) gaussian functions: name, title, observable, mean, sigma
gaus = RooGaussian("gaus", "Gaussian distribution", x, mean, sigma)

## (b) poisson distribution:
pois = RooPoisson("pois", "Poisson distribution", x, mean)

## (c) polynomial distributions: normalize polynomial. Normalization range is taken from
##                               range of x variable (i.e. 0..100 in our example)
pol1 = RooPolynomial("pol1", "Linear distribution", x, RooArgSet(slope))
pol2 = RooPolynomial("pol2", "Quadratic distribution", x, RooArgSet(slope, a2))

## NOTE: parameters are passed to the  RooPolynomial using the RooArgSet class. This class 
##       can wrap up arbitrary number of optional parameters (well, up to 9) so that they can
##       be passed into RooPolynomial as a single parameter. We will see this approach many times
##       in RooFit.

## (d) exponential function:
expo = RooExponential("expo", "exp^{cx}", x, c)

##---------------------------------  
## STEP 3: display PDFs

## create canvas (ordinary root TCanvas)
c1 = TCanvas("c1", "PDF example: simple PDFs", 200, 200, 800, 600)

## create RooPlot class. This class can display data and PDFs on top of each other
frame1 = x.frame()

## draw our example PDFs onto the frame1
gaus.plotOn(frame1, RooFit.LineColor(kRed), RooFit.LineWidth(3))
## NOTE: classes LineColor, LineWidth are so called "modifiers". They are used to pass 
##       optional settings to the plotOn method. They can go in any order. 

pois.plotOn(frame1, RooFit.LineColor(kGreen), RooFit.LineWidth(3)) ## plot the composite PDF

## plot the PDF's constituents, i.e. gaussian and polynomial

frame1.Draw() ## draw frame1 onto the root canvas

##---------------------------------  
## STEP 4: composite PDFs
##         addition:       f * a(x) + (1-f)*b(x)      -- class RooAddPdf
##         multiplication: a(x) * b(x)                -- class RooProdPdf
##         convolution:    int[ dy a(y) * b(x-y) ]    -- class RooFFTConvPdf (numerical convolution)

## addition **************
model1 = RooAddPdf("model1", "gaus+polynomial PDF", RooArgSet(gaus, pol1), RooArgSet(fSig))

c2 = TCanvas("c2", "PDF example: sum of 2 PDFs", 200, 200, 800, 600)
frame2 = x.frame()

## plot the main PDF (i.e. model1)
model1.plotOn(frame2, RooFit.LineColor(kBlue), RooFit.LineWidth(3))

## plot constituent PDFs, i.e. "gaus" and "pol1"
model1.plotOn(frame2, RooFit.LineColor(kRed), RooFit.LineWidth(3), RooFit.LineStyle(kDashed), RooFit.Components(gaus))
model1.plotOn(frame2, RooFit.LineColor(kGreen), RooFit.LineWidth(3), RooFit.LineStyle(kDashed), RooFit.Components(pol1))

frame2.Draw()

## multiplication **************
## example: 2D gaussian (uncorrelated). We will need new observable "y" and new mean_y and sigma_y parameters:
y       = RooRealVar("y", "y", 0, -10, 150)
mean_y  = RooRealVar("mean_y", "#mu_{y}", 60)
sigma_y = RooRealVar("sigma_y", "#sigma_{y}", 20, 1e-5 , 150)

## gaussian in y-variable
gaus_y = RooGaussian("gaus_y", "Gaussian distribution (y)", y, mean_y, sigma_y)

## define PDF as a product gaus * gaus_y:
model2 = RooProdPdf("model2", "2D gaus PDF", gaus, gaus_y)

c3 = TCanvas("c3", "PDF example: product of 2 PDFs", 200, 200, 800, 600)

## create TH1 class from our model:
hh_model2 = model2.createHistogram("hh_model",x,RooFit.Binning(50),RooFit.YVar(y,RooFit.Binning(50))) 
hh_model2.SetLineColor(kBlue)

## draw the model
hh_model2.Draw("surf")


## projection to Y axis (integral over X)
c4 = TCanvas("c4", "PDF example: projection", 200, 200, 800, 600)
frame3 = y.frame()

## plot the main PDF (i.e. model1)
model2.plotOn(frame3, RooFit.LineColor(kBlue), RooFit.LineWidth(3))
frame3.Draw()  

##---------------------------------  
## STEP 5: conditional probability: 
## example gaussian: width of the gaussian can be though of as a conditional variable:
##   gaus(x|sigma)
## We can then define 2D PDF if we know PDF of sigma: model3 = gaus(x|sigma) * w(sigma)
## In our example we will use w(sigma) = exp(a*sigma)

wSigma = RooExponential("wSigma", "exp^{c*sigma}", sigma, c)

model3 = RooProdPdf("model3", "model3", wSigma, RooFit.Conditional(gaus, x))
## NOTE: Modifier "Conditional" tells the RooProdPdf that the gaussian is to be taken as a conditional PDF gaus(x|sigma)


## draw 2D function
c5 = TCanvas("c5", "PDF example: conditional PDF", 200, 200, 800, 600)

## create TH1 class from our model:
hh_model3 = model3.createHistogram("hh_model3",x,RooFit.Binning(50),RooFit.YVar(sigma,RooFit.Binning(50))) 
hh_model3.SetLineColor(kRed)
hh_model3.Draw("surf")

## draw projection of model3 onto x axis:
c6 = TCanvas("c6", "PDF example: projection", 200, 200, 800, 600)
frame4 = x.frame()
model3.plotOn(frame4, RooFit.LineColor(kBlack), RooFit.LineWidth(3))
frame4.Draw()  
   
   
   
  
