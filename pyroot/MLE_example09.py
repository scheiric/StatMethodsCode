"""
 example of extended likelihood fit
"""
from ROOT import RooDataSet, RooRealVar, RooGaussian, RooChebychev, RooAddPdf, RooPlot, RooDataSet, TCanvas, TH1D, RooArgSet
from ROOT import RooFit, kBlue, kYellow, kWhite, kBlack, kRed, kMagenta, kCyan, kBlue, kGreen, kDashed

nEvt = 300

m = RooRealVar("m", "m", 0, 100, 150, "GeV") ## mass

##---------------------------------  
## define model parameters. Again, class RooRealVar is used.
##---------------------------------  

## examples:
mean  = RooRealVar("mean", "#mu", 125, 80, 200, "GeV") ## if no range is given, the value is fixed
sigma = RooRealVar("sigma", "#sigma", 2., 0, 10)
fSig  = RooRealVar("fSig", "f_{sig}", 0.2, 0, 1)
slope = RooRealVar("slope", "a_{1}", -0.8, -1, 1)

ns = RooRealVar("ns", "ns", fSig.getVal()*nEvt, 0, 1000000)
nb = RooRealVar("nb", "nb", (1-fSig.getVal())*nEvt, 0, 1000000)

sigPdf = RooGaussian ("sigPdf", "sigPdf", m, mean, sigma)
bkgPdf = RooChebychev("bkgPdf", "bkgPdf", m, RooArgSet(slope))

model1 = RooAddPdf("model1", "f sigPdf + (1-f) bkgPdf", RooArgSet(sigPdf, bkgPdf), RooArgSet(fSig))
model2 = RooAddPdf("model2", "ns sigPdf + nb bkgPdf", RooArgSet(sigPdf, bkgPdf), RooArgSet(ns,nb))

##---------------------------------  
## generate toy data
##---------------------------------  
data = model1.generate(m, nEvt)

##---------------------------------   
## display initial PDFs
##---------------------------------  

## create canvas (ordinary root TCanvas)
c1 = TCanvas("c1", "initial PDF", 200, 200, 800, 600)

## create RooPlot class. This class can display data and PDFs on top of each other
frame1 = m.frame() ## NOTE: x.frame() method returns POINTER!

## draw data and PDFs onto the frame1
data .plotOn(frame1, RooFit.Binning(30))
model1.plotOn(frame1, RooFit.LineColor(kRed), RooFit.LineWidth(3))

## plot the PDF's constituents, i.e. gaussian and polynomial
frame1.Draw() ## draw frame1 onto the root canvas
  
##---------------------------------   
## perform fit
##---------------------------------  
model1.fitTo(data)
model2.fitTo(data, RooFit.Extended(True))

##---------------------------------  
## display
##---------------------------------  
c2 = TCanvas("c2", "fitted PDF (non-extended)", 200, 200, 800, 600)
frame2 = m.frame()

## draw data and PDFs onto the frame1
data .plotOn(frame2, RooFit.Binning(30))
model1.plotOn(frame2, RooFit.LineColor(kBlue), RooFit.LineWidth(3))
frame2.Draw()

##---------------------------------  
## display
##---------------------------------  
c3 = TCanvas("c3", "fitted PDF (extended)", 200, 200, 800, 600)
frame3 = m.frame()

## draw data and PDFs onto the frame1
data .plotOn(frame3, RooFit.Binning(30))
model2.plotOn(frame3, RooFit.LineColor(kBlue), RooFit.LineWidth(3))
frame3.Draw()

print("fSig            = ", fSig.getVal(), " +/- " ,  fSig.getError())
print("Calculated Ns   = ", nEvt*fSig.getVal() ,  " +/- " ,  nEvt*fSig.getError())
print("Calculated Nb   = ", nEvt*(1-fSig.getVal()) ,  " +/- " ,  nEvt*fSig.getError())

## from extended likelihood fit
print("ns = " ,  ns.getVal() ,  " +/- " ,  ns.getError())
print("nb = " ,  nb.getVal() ,  " +/- " ,  nb.getError())


