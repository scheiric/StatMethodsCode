"""
Example of the TMVA package (Multivariate analysis)
for event clasification
Example 3: application of the trained MVA clasifiers on real DATA

 Make sure you run the MVA_example01.py beforehand


"""

from ROOT import TChain, TFile, TString, TTree, TCanvas, TString, TObjString, TSystem, TROOT, TMath, TLegend, TH2F, TMVA, TH1F, kBlack, kRed, kMagenta, kGreen, kWhite
from array import array
import math

## This loads the library
TMVA.Tools.Instance()

## --- Create the Reader object
reader = TMVA.Reader( "!Color:!Silent" )    

## Create a set of variables and declare them to the reader
## - the variable names MUST corresponds in name and type to those given in the weight file(s) used
fChi2  = array('f', [0])
lxySig = array('f', [0])
a0Sig  = array('f', [0])
pt     = array('f', [0])
reader.AddVariable( "chi2",   fChi2 )
reader.AddVariable( "lxySig", lxySig )
reader.AddVariable( "a0Sig",  a0Sig )
reader.AddVariable( "pt",     pt )

## Spectator variables declared in the training have to be added to the reader, too
fMass = array('f', [0])
reader.AddSpectator( "mass",  fMass )

## --- Book the MVA methods
reader.BookMVA( "Cuts method",   "dataset/weights/TMVAClassification_Cuts.weights.xml" ) 
reader.BookMVA( "Fisher method", "dataset/weights/TMVAClassification_Fisher.weights.xml" ) 
reader.BookMVA( "BDT method",    "dataset/weights/TMVAClassification_BDT.weights.xml" ) 
reader.BookMVA( "MLP method",    "dataset/weights/TMVAClassification_MLP.weights.xml" ) 

## mass distributions:
histAll_mass  = TH1F( "mass", "All events;m (MeV)N", 35, 1100, 1132 )
histCuts_mass = TH1F( "histCuts_mass", "MVA_Cuts events;m (MeV)N", 35, 1100, 1132 )
histFi_mass   = TH1F( "mass_Fisher", "MVA_Fisher;m (MeV)N", 35, 1100, 1132 )
histBdt_mass  = TH1F( "mass_BDT", "MVA_BDT;m (MeV)N", 35, 1100, 1132 )
histNn_mass   = TH1F( "mass_MLP", "MVA_MLP;m (MeV)N", 35, 1100, 1132 )

pt_vs_a0Sig_Cuts_S   = TH2F( "pt_vs_a0Sig_Cuts_S", "MVA_CUTS;p_{T} [MeV];a_{0}/#sigma;N", 100, 0,5000, 100, 0,3 )
pt_vs_a0Sig_Cuts_B   = TH2F( "pt_vs_a0Sig_Cuts_B", "MVA_CUTS;p_{T} [MeV];a_{0}/#sigma;N", 100, 0,5000, 100, 0,3 )
pt_vs_a0Sig_Fi_S     = TH2F( "pt_vs_a0Sig_Fi_S"  , "MVA_Fi;p_{T} [MeV];a_{0}/#sigma;N"  , 100, 0,5000, 100, 0,3 )
pt_vs_a0Sig_Fi_B     = TH2F( "pt_vs_a0Sig_Fi_B"  , "MVA_Fi;p_{T} [MeV];a_{0}/#sigma;N"  , 100, 0,5000, 100, 0,3 )
pt_vs_a0Sig_BDT_S    = TH2F( "pt_vs_a0Sig_BDT_S" , "MVA_BDT;p_{T} [MeV];a_{0}/#sigma;N" , 100, 0,5000, 100, 0,3 )
pt_vs_a0Sig_BDT_B    = TH2F( "pt_vs_a0Sig_BDT_B" , "MVA_BDT;p_{T} [MeV];a_{0}/#sigma;N" , 100, 0,5000, 100, 0,3 )
pt_vs_a0Sig_MLP_S    = TH2F( "pt_vs_a0Sig_MLP_S" , "MVA_MLP;p_{T} [MeV];a_{0}/#sigma;N" , 100, 0,5000, 100, 0,3 )
pt_vs_a0Sig_MLP_B    = TH2F( "pt_vs_a0Sig_MLP_B" , "MVA_MLP;p_{T} [MeV];a_{0}/#sigma;N" , 100, 0,5000, 100, 0,3 )

## plots for MC
pt_vs_a0Sig_MC_S    = TH2F( "pt_vs_a0Sig_MC_S" , "MVA_MC;p_{T} [MeV];a_{0}/#sigma;N" , 100, 0,5000, 100, 0,3 )
pt_vs_a0Sig_MC_B    = TH2F( "pt_vs_a0Sig_MC_B" , "MVA_MC;p_{T} [MeV];a_{0}/#sigma;N" , 100, 0,5000, 100, 0,3 )


## read DATA file (this could be the real data file, but we will use our MC file again)
input = TFile.Open( "data/skimThin.mc10.Lam+LamBar.MinBias.SimpleTree.01.root" )
t     = input.Get("Lam")  ## TTree for Lam.p+pi decays

## setup tree branches  
## loop over events in the input tree
for i in range(t.GetEntries()):
  if i%50000==0: print("event ", i, "/", t.GetEntries())
  
  ## abort after 200000 events so we don't have to wait so long
  if i>200000: break

  ## read event    
  t.GetEntry(i)
  
  ## loop over Lam candidates in each single event
  for j in range(t.VTX_chi2.size()):

    ## fill the TMVA variables
    fChi2[0]  =  t.VTX_chi2.at(j)                                      ## vertex fit min chi2
    lxySig[0] =  t.VTX_lxy.at(j)[0]/t.VTX_lxyError.at(j)[0]            ## decay distance significance
    a0Sig[0]  =  t.VTX_a0.at(j)[0]/t.VTX_a0Error.at(j)[0]              ## impact parameter significance
    pt[0]     =  math.sqrt(t.VTX_px.at(j) ** 2 + t.VTX_py.at(j) ** 2)  ## transverse momentum
    fMass[0]  = t.VTX_mass.at(j)                                      ## reconstructed mass
    
    ## --- fill the mass distribution:
    histAll_mass.Fill(fMass[0])
    
    ## optimized rectangular cuts:
    if fChi2[0]<1.6443 and lxySig[0]>8.10443 and a0Sig[0] < 1.78863 and pt[0] > 905.411:
      histCuts_mass.Fill(fMass[0])
      pt_vs_a0Sig_Cuts_S.Fill(pt[0], a0Sig[0])
    else:
      pt_vs_a0Sig_Cuts_B.Fill(pt[0], a0Sig[0])
    
    
    ## apply cut on the value of the Fisher discriminant.
    if reader.EvaluateMVA( "Fisher method") > -0.021:
      histFi_mass.Fill(fMass[0])
      pt_vs_a0Sig_Fi_S.Fill(pt[0], a0Sig[0])
    else:
      pt_vs_a0Sig_Fi_B.Fill(pt[0], a0Sig[0])
    

    ## apply cut on the output from the boosted decision tree
    if reader.EvaluateMVA( "BDT method") > 0.057:
      histBdt_mass.Fill(fMass[0])
      pt_vs_a0Sig_BDT_S.Fill(pt[0], a0Sig[0])
    else:
      pt_vs_a0Sig_BDT_B.Fill(pt[0], a0Sig[0])

    
    ## apply cut on the output from the artifitial neural network
    if reader.EvaluateMVA( "MLP method") > 0.593 :
      histNn_mass.Fill(fMass[0])
      pt_vs_a0Sig_MLP_S.Fill(pt[0], a0Sig[0])
    else:
      pt_vs_a0Sig_MLP_B.Fill(pt[0], a0Sig[0])



## read MC file
inputMC = TFile.Open( "data/skimThin.mc10.Lam+LamBar.MinBias.SimpleTree.01.root" )
tMC     = inputMC.Get("Lam")  ## TTree for Lam.p+pi decays

## loop over events in the input tree
for i in range(tMC.GetEntries()):
  if i%50000==0: print("event ", i, "/", tMC.GetEntries())
  
  ## abort after 200000 events so we don't have to wait so long
  if i>200000: break

  ## read event    
  tMC.GetEntry(i)
  
  ## loop over Lam candidates in each single event
  for j in range(tMC.VTX_chi2.size()):
    ## fill the TMVA variables
    fChi2[0]  =  tMC.VTX_chi2.at(j)                                        ## vertex fit min chi2
    lxySig[0] =  tMC.VTX_lxy.at(j)[0]/tMC.VTX_lxyError.at(j)[0]            ## decay distance significance
    a0Sig[0]  =  tMC.VTX_a0.at(j)[0]/tMC.VTX_a0Error.at(j)[0]              ## impact parameter significance
    pt[0]     =  math.sqrt(tMC.VTX_px.at(j) ** 2 + tMC.VTX_py.at(j) ** 2)  ## transverse momentum
    fMass[0]  = tMC.VTX_mass.at(j)                                         ## reconstructed mass
    
    ## signal/background clasification based on MC truth:
    if tMC.isSignal.at(j):
      pt_vs_a0Sig_MC_S.Fill(pt[0], a0Sig[0])
    else:
      pt_vs_a0Sig_MC_B.Fill(pt[0], a0Sig[0])

## display mass distribution
c1 = TCanvas("c1", "All events", 200, 200, 800, 600)
histAll_mass.SetMinimum(0)
histAll_mass.Draw()

c2 = TCanvas("c2", "Mass of selected events", 200, 200, 800, 600)
histCuts_mass.SetLineColor(kBlack)
histCuts_mass.SetLineWidth(3)

histFi_mass.SetLineColor(kRed)
histFi_mass.SetLineWidth(3)

histBdt_mass.SetLineColor(kMagenta)
histBdt_mass.SetLineWidth(3)
histBdt_mass.SetLineStyle(3)

histNn_mass.SetLineColor(kGreen-2)
histNn_mass.SetLineWidth(3)

histNn_mass.Draw()
histCuts_mass.Draw("same")
histFi_mass.Draw("same")
histBdt_mass.Draw("same")

l = TLegend(0.1,0.9,0.35,0.6)
l.AddEntry(histCuts_mass , "Cuts", "l")
l.AddEntry(histFi_mass , "Fisher", "l")
l.AddEntry(histBdt_mass , "BDT", "l")
l.AddEntry(histNn_mass , "ANN", "l")

l.SetFillColor(kWhite)
l.Draw("same")

## draw regions in lxySig and a0Sig space which were selected as signal and background:
c4 = TCanvas("c4", "TMVA selection regions", 200, 200, 800, 600)
c4.Divide(3,2)

## MC truth selection  
c4.cd(1)
pt_vs_a0Sig_MC_S.SetMarkerColor(kGreen)
pt_vs_a0Sig_MC_B.SetMarkerColor(kRed)
pt_vs_a0Sig_MC_S.SetMarkerStyle(1)
pt_vs_a0Sig_MC_B.SetMarkerStyle(1)
pt_vs_a0Sig_MC_B.Draw()
pt_vs_a0Sig_MC_S.Draw("same")

c4.cd(2)
## for rectangular cuts
pt_vs_a0Sig_Cuts_S.SetMarkerColor(kGreen)
pt_vs_a0Sig_Cuts_B.SetMarkerColor(kRed)
pt_vs_a0Sig_Cuts_S.SetMarkerStyle(1)
pt_vs_a0Sig_Cuts_B.SetMarkerStyle(1)
pt_vs_a0Sig_Cuts_B.Draw()
pt_vs_a0Sig_Cuts_S.Draw("same")

## for Fisher's discriminant
c4.cd(3)
pt_vs_a0Sig_Fi_S.SetMarkerColor(kGreen)
pt_vs_a0Sig_Fi_B.SetMarkerColor(kRed)
pt_vs_a0Sig_Fi_S.SetMarkerStyle(1)
pt_vs_a0Sig_Fi_B.SetMarkerStyle(1)
pt_vs_a0Sig_Fi_B.Draw()
pt_vs_a0Sig_Fi_S.Draw("same")

## for BDT
c4.cd(4)
pt_vs_a0Sig_BDT_S.SetMarkerColor(kGreen)
pt_vs_a0Sig_BDT_B.SetMarkerColor(kRed)
pt_vs_a0Sig_BDT_S.SetMarkerStyle(1)
pt_vs_a0Sig_BDT_B.SetMarkerStyle(1)
pt_vs_a0Sig_BDT_B.Draw()
pt_vs_a0Sig_BDT_S.Draw("same")

## form ANN
c4.cd(5)
pt_vs_a0Sig_MLP_S.SetMarkerColor(kGreen)
pt_vs_a0Sig_MLP_B.SetMarkerColor(kRed)
pt_vs_a0Sig_MLP_S.SetMarkerStyle(1)
pt_vs_a0Sig_MLP_B.SetMarkerStyle(1)
pt_vs_a0Sig_MLP_B.Draw()
pt_vs_a0Sig_MLP_S.Draw("same")
  
  

