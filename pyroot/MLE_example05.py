"""
Example 5: pul distribution
"""


from ROOT import  RooDataSet, RooRealVar, RooPolynomial, RooPlot, RooMinimizer, RooMinuit, RooFitResult, TFile, TTree, TCanvas, TGraph, TH2D, TLine, TLegend, TPaveText, RooArgSet, RooFit, TH1D
from ROOT import kBlue, kYellow, kWhite, kBlack

## define observable: RooFit class RooRealVar
x = RooRealVar("x", "x", 0, -0.95, 0.95); ## value, range_min, range_max (N.B. the value doesn't matter here)

## Load the TTree form the input file
## NOTE: it is assumed that the "scatter.root" file is already created 
##       by running lesson3_example03.py
inFile = TFile.Open("scatter.root")
t = inFile.Get("scatter")

## convert data to the RooFit class RooDataSet. The imported tree must have a branch named "x"
data = RooDataSet("data", "data", t, RooArgSet(x))

############################/
## define fit model and perform fit
############################/

## Parameters: again RooRealVar classes
alpha = RooRealVar ("alpha", "#alpha", 0.8, -2, 2) ## initial value, range_min, range_max
beta  = RooRealVar ("beta",  "#beta",  0.8, -2, 2) ## fixed wrong initial value

##  RooRealVar alpha("alpha", "#alpha", 0.8, 0.55, 2) ## tighter range
##  RooRealVar beta ("beta",  "#beta",  1.25, 1.25, 1.25) ## fixed wrong initial value

## model: using RooPolynomial class
model = RooPolynomial("model", "1+alpha*x + beta*x^2", x, RooArgSet(alpha, beta))

## perform the fit:
result = model.fitTo(data, RooFit.Save(True))

## plot the result: using RooFit class RooPlot
c1 = TCanvas("c1", "ML fit", 0, 10, 800, 600)

frame_final = x.frame()
data .plotOn(frame_final, RooFit.Binning(50), RooFit.LineColor(kBlack), RooFit.LineWidth(2), RooFit.MarkerSize(1.2), RooFit.XErrorSize(1.))
model.plotOn(frame_final, RooFit.LineColor(kBlue), RooFit.LineWidth(3))

frame_final.Draw()

############################/
## do pseudo-experiments
############################/
h_Lmax = TH1D("h_Lmax", "Estimated districtution of Lmax; L_{max}", 100, -650, -550)

h_alpha_pull = TH1D("h_alpha_pull", "Pull distribution for #alpha; (#hat{#alpha}_{j} - #hat{#alpha})/#sigma_{#alpha_{j}}", 100, -5, -5)
h_beta_pull = TH1D("h_beta_pull", "Pull distribution for #alpha; (#hat{#beta}_{j} - #hat{#beta})/#sigma_{#beta_{j}}", 100, -5, -5)

nexp = 1000
nEvt = t.GetEntries() ## size of the sample 
alphaHat = alpha.getVal() ## estimated alpha value
betaHat  = beta.getVal() ## estimated beta value

for i in range(nexp):
    ## reset parameters to the estimated values from the 1st fit
    alpha.setVal(alphaHat)
    beta .setVal(betaHat)

    ## generate pseudo-data using RooFit:
    pseudo_data = model.generate(x, nEvt)

    ## perform fit on pseudo-data
    pseudo_result = model.fitTo(pseudo_data, RooFit.Save(True)) ## Save(kTrue) means the results are stored

    ## fill the histograms
    h_Lmax.Fill(-pseudo_result.minNll()) ##minNll returns -log L_max, hence the minus
    h_alpha_pull.Fill((alpha.getVal()-alphaHat)/alpha.getError())
    h_beta_pull.Fill((beta.getVal()-betaHat)/beta.getError())

    ## cleanup
    del pseudo_result
    del pseudo_data


## normalize histogram:
h_Lmax.Scale(1./h_Lmax.GetEntries())

## draw
c2 = TCanvas("c2","c2", 0, 10, 800, 600)

## calculate P-value (note: we are rounding to the whole bins. This is sufficient)
bin = h_Lmax.FindBin(-result.minNll())
h_pvalue = h_Lmax.Clone("h_pvalue")

for i in range(bin+1, h_pvalue.GetNbinsX()+2):
    h_pvalue.SetBinContent(i, 0)

h_pvalue.SetFillColor(kYellow)
h_pvalue.Draw("hist")
h_Lmax.Draw("same")
print("P-Value = ", h_pvalue.Integral())

## draw the observed value of Lmax
l = TLine(-result.minNll(), 0, -result.minNll(), h_Lmax.GetMaximum())
l.SetLineStyle(2)
l.Draw()

c3 = TCanvas("c3","c3", 0, 10, 800, 600)
h_alpha_pull.Draw()
h_alpha_pull.SetStats(0)
h_alpha_pull.SetMaximum(1.4*h_alpha_pull.GetMaximum())

stats1 = TPaveText(0.6, 0.9, 0.9, 0.75, "NDC")
stats1.SetFillColor(kWhite)
stats1.AddText("Mean = {:.2f} #pm {:.2f}".format(h_alpha_pull.GetMean(), h_alpha_pull.GetMeanError()))
stats1.AddText("RMS = {:.2f} #pm {:.2f}".format(h_alpha_pull.GetRMS(), h_alpha_pull.GetRMSError()))
stats1.Draw("same")
c3.Modified()
c3.Update()

c4 = TCanvas("c4","c4", 0, 10, 800, 600)
h_beta_pull.Draw()
h_beta_pull.SetStats(0)
h_beta_pull.SetMaximum(1.4*h_beta_pull.GetMaximum())
stats2 = TPaveText(0.6, 0.9, 0.9, 0.75, "NDC")
stats2.SetFillColor(kWhite)
stats2.AddText("Mean = {:.2f} #pm {:.2f}".format(h_beta_pull.GetMean(), h_beta_pull.GetMeanError()))
stats2.AddText("RMS = {:.2f} #pm {:.2f}".format(h_beta_pull.GetRMS(), h_beta_pull.GetRMSError()))
stats2.Draw("same")
c4.Modified()
c4.Update()
  

