""" Hypo-test example 01: template fit
"""

from configManager import configMgr
from configWriter import Sample
from systematic import Systematic

from ROOT import TFile, kRed, kBlue, kOrange, kGreen, kYellow
import math
import os

## some fit parameters
fitName = "TheFit02"

# ----------------------------------------
# Setup configMgr
# ----------------------------------------
configMgr.calculatorType = 2  # 2 = Asymptotic, 0 = frequentist with toys
configMgr.testStatType = 3    # 3 = one-sided profile likelihood
configMgr.nPoints = 20        # hypo-test inversion done with 20 point scan

# we will not use blinding
configMgr.blindSR = False
configMgr.blindCR = False
configMgr.blindVR = False

configMgr.inputLumi  = 1. 
configMgr.outputLumi = 1. * configMgr.inputLumi
configMgr.setLumiUnits("fb-1")

configMgr.analysisName           = fitName
configMgr.outputFileName         = "results/{}_Output.root".format(configMgr.analysisName)
configMgr.ReduceCorrMatrix       = True # only plot corelations >10%

configMgr.useCacheToTreeFallback = False
configMgr.nomName = "_nominal" # Nominal TTree suffix


# ----------------------------------------
# Create the fitConfig instance
# ----------------------------------------
# Define top-level
fit = configMgr.addFitConfig(fitName)

# ----------------------------------------
# Create channels (i.e. fit regions)
# ----------------------------------------
# in this example, we will only have one channel: Signal Region (SR)
varName = 'S_T'           # name of the variable used in the fit. In this case a scalar sum of the final-state object pT
nBins = 6                 # number of histogram bins used in the fit
xRange = (200, 1400)      # histogram x-axis range
binSize = float(xRange[1]-xRange[0])/nBins

# ----------------------------------------
# Regin function
# ----------------------------------------
def rebin(hist, nBins, xMin, xMax):
  """ Rebins TH1 histgram into a desired uniform binning and returns the 
      bin values and uncertainties as twi lists.
      Under-flow and over-flow bins are added to the first and last bins of the new histogram
  """
  ## prepare the empty histogram as a list
  outHist = [0.] * nBins
  outErr = [0.] * nBins

  ## get the new bin size
  binSize = float(xMax - xMin) / nBins

  ## loop over bins of the old histogram
  for b in range(1, hist.GetNbinsX()+1):
    binCenter = hist.GetBinCenter(b)
    value = hist.GetBinContent(b)
    wSq = hist.GetBinError(b) ** 2

    ## check into which bin of the new histogram it falls
    for i in range(nBins):
      binLoEdge = xMin + i * binSize
      binHiEdge = xMin + (i+1) * binSize
      belongsToBin = binCenter>=binLoEdge and binCenter<binHiEdge
      isUndeflow = i==0 and binCenter<binLoEdge
      isOverflow = i+1==nBins and binCenter>=binHiEdge
      if belongsToBin or isUndeflow or isOverflow:
        outHist[i] += value
        outErr[i]  += wSq

  ## calculate square-root of the sum-of-weights squared
  outErr = [ math.sqrt(w2) for w2 in outErr]

  ## return the pair
  return (outHist, outErr)

# ----------------------------------------
# Add samples into the region
# ----------------------------------------
# we will open the root files and read the histograms corresponding to the samples
sampleNames = [('LQ500',     kRed), # Leptoquark, a hypothetical particle we are trying to find
               ('Ztt',       kBlue+2), # Background: Z->tautau decays
               ('Zll',       kBlue), # Background: Z->ee or Z->mumu decays
               ('ttbar',     kOrange), # Background: t and anti-t quark pairs
               ('singleTop', kOrange-2), # Background: single-t quark
               ('VV',        kGreen), # Backrgound: WW, WZ, and ZZ boson pairs
               ('Fakes',     kYellow), # Backrgound: events with mis-reconstructed final states
               ]

# We want the signal sample, the ttbar+singleTop, and the Ztt+Zll samples normalized 
# with the free-floating norm factor in the fit.
# We will define them using the dict:
normFactors = {
  'LQ500': 'mu',  # the signal strength
  'Ztt' : 'NF_Z',  # Z norm. factor
  'Zll' : 'NF_Z',  # the same norm factor as for Ztt. 
  'ttbar' : 'NF_t',  # top norm. factor
  'singleTop' : 'NF_t',  # the same norm factor as for ttbar. 
}

## we will consider the following regions:
regionNames = ['sr',      ## signal region
               'cr_top2cr', ## CR to constrain top quark background
               'cr_ztt',  ## CR to constrain Z boson background
               ]

## Systematics: adding only selected few systematics
## The provided names are names of the histograms in the input file that contain the varied event yields
systematics = [
  ('JET_EffectiveNP_1', '__1up', '__1down' ), # jet energy scale uncertainty
  ('TAUS_TRUEHADTAU_SME_TES_DETECTOR', '__1up', '__1down'), # tau energy scale uncertainty
  ('TAUS_TRUEHADTAU_EFF_RECO_TOTAL', '__1up', '__1down'), # tau reconstruction efficiency uncertainty
  ('theory_V_ckkw', '_1up', '_1down'), # Z boson theory model uncertainty
  ('theory_t_me',), # top quark theory model uncertainty
  ('theory_t_fs_ds', ), # top quark theory model uncertainty (interference between ttbar and tW)
]



# open the input file:
# This file was created for the purpose of this example. It contains several analysis regions, which contain
# folders for several MC signal and background samples, which in turn contain histograms of the sensitive variable
# with the nominal event yields and with yields after systematic variations.
# There are many samples and systematics, we will only use few of them in our example.
f = TFile.Open("data/wsi_ST_allsyst.root")

## channels:
channels = []

## loop over fit regions
for regionName in regionNames:
  print("Adding region {}".format(regionName))
  chan = fit.addChannel('S_T', [regionName], nBins, xRange[0], xRange[1])

  # Add selection: not really used when working with histograms as inputs, but we have to put in something
  configMgr.cutsDict[regionName] = "true"

  ## get the histogram representing measured data in this example
  h_data = f.Get("chan_{}__cat_presel__{}/Data/nominal".format('hh' if 'top' not in regionName else '1tau', regionName))

  # now we rebin the histograms into a desired binning. Over-flow and onder-flow bins will be added
  # into the first and the last bin. This is a common practice!
  binValues, binErrors = rebin(h_data, nBins, xRange[0], xRange[1])

  ## create the data sample and add it to the region
  dataSample = Sample("Data")
  dataSample.setData()
  # dataSample.buildHisto(binValues, regionName, varName, xRange[0], binSize)
  dataSample.buildHisto(binValues, regionName, varName, xRange[0], binSize)
  dataSample.buildStatErrors(binErrors, regionName, varName)
  chan.addSample(dataSample)
  print("Added sample {}: {}".format(dataSample.name, binValues))

  ## add MC samples that will be fitted into data
  for sampleName, sampleColor in sampleNames:
    # load the histogram with nominal event yields
    h_nominal = f.Get("chan_{}__cat_presel__{}/{}/nominal".format('hh' if 'top' not in regionName else '1tau', regionName, sampleName))
    
    # now we rebin the histograms into a desired binning. Over-flow and onder-flow bins will be added
    # into the first and the last bin. This is a common practice!
    binValues, binErrors = rebin(h_nominal, nBins, xRange[0], xRange[1])

    ## create the "sample" instance
    sample = Sample(sampleName, sampleColor)
    # sample.buildHisto(binValues, regionName, varName, xRange[0], binSize)
    sample.buildHisto(binValues, regionName, varName, xRange[0], binSize)
    sample.buildStatErrors(binErrors, regionName, varName)
    sample.setStatConfig(True)

    # add norm factor if requested:
    if sampleName in normFactors:
      sample.setNormFactor(normFactors[sampleName], 1, -5., 5.) # name, start value, allowed range
    else:
      sample.setNormByTheory()

    ## systematic uncertainties
    for systName in systematics:
      # load the histogram with varied event yields. Some systematics have independent up and down variations. 
      # Others are only one-sided and have to be symmetrized by hand
      if len(systName)==1:
        h_up = f.Get("chan_{}__cat_presel__{}/{}/{}".format('hh' if 'top' not in regionName else '1tau', regionName, sampleName, systName[0]))
        h_down = None
      else:
        h_up = f.Get("chan_{}__cat_presel__{}/{}/{}{}".format('hh' if 'top' not in regionName else '1tau', regionName, sampleName, systName[0], systName[1]))
        h_down = f.Get("chan_{}__cat_presel__{}/{}/{}{}".format('hh' if 'top' not in regionName else '1tau', regionName, sampleName, systName[0], systName[2]))

      # check if the systematic variation exists. If not, skip it. This can happen e.g. for theory systematics which are only applicable for some samples
      if not h_up: continue

      # rebin the histograms. NOTE: we do not care about statistical uncertainties of the systematic variations
      binValues_up, _ = rebin(h_up, nBins, xRange[0], xRange[1])
      if h_down:
        binValues_down, _ = rebin(h_down, nBins, xRange[0], xRange[1])

      ## symmetrize
      if not h_down:
        binValues_down = [ 2*nom - v for nom, v in zip(binValues, binValues_up) ]

      ## turn into relative uncertainty
      binValues_up = [ v/nom if nom!=0 else 0. for nom, v in zip(binValues, binValues_up) ]
      binValues_down = [ v/nom if nom!=0 else 0. for nom, v in zip(binValues, binValues_down) ]

      # create the systematic instance
      syst = Systematic(systName[0], None, binValues_up, binValues_down, "user", "userHistoSys")
      print(binValues_up, binValues_down)
      sample.addSystematic(syst)

    # add to the channel
    chan.addSample(sample)
    print("Added sample {}: {}".format(sample.name, binValues))

    ## set signal sample
    if 'LQ' in sampleName:
      fit.setSignalSample(sample)

  # add region (channel) into the list
  channels.append(chan)


# pass channels to the fit.
fit.addBkgConstrainChannels(channels)

# ----------------------------------------
# Define measurement
# ----------------------------------------
meas = fit.addMeasurement("NormalMeasurement", lumi=1., lumiErr=0.03)

# add patameter of interest: the signal strength "mu"
meas.addPOI("mu")

# ----------------------------------------
# Cleanup
# ----------------------------------------
# These lines are needed for the user analysis to run
# Make sure file is re-made when executing HistFactory
if configMgr.executeHistFactory:
    if os.path.isfile("data/{}.root".format(configMgr.analysisName)):
        os.remove("data/{}.root".format(configMgr.analysisName)) 
