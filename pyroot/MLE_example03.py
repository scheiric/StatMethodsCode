""" Example 3: 
 a/ hit-and-miss (von Neuman rejection) MC method
 b/ ML fit for function of 2 parameters (implemented using ROOT minuit interface)
 
 f(x, alfa, beta) = (1 + alfa*x + beta*x*x)/ (2 + 2*beta/3)
 
 For limited range [xmin, xmax], the normalized function is
 f(x, alfa, beta) = (1 + alfa * x + beta * x*x)/
 ((xmax - xmin) + 0.5*alfa*( xmax^2 - xmin^2) + beta/3*(xmax^3-xmin^3))
 
 Examples: ee-> mumu scattering: alfa = 0, beta = 1
 
"""

from ROOT import TH1D, TH2D, TRandom3, TCanvas, TStyle, TFile, TTree, gStyle
from ROOT import Minuit2
from ROOT import Math
from array import array
import math


## Define input paramters
xmin = -0.95
xmax =  0.95
normal  = (xmax - xmin)
normal2 = 0.5*(xmax*xmax - xmin*xmin)
normal3 = 1/3.*(xmax*xmax*xmax - xmin*xmin*xmin)

## Define the fitted function
def f_scatter( x, alfa, beta):
   ## normalization unimportant for hit and miss, usefull for likelihoods
   return ( 1 + alfa*x + beta* x*x) / ( normal + alfa*normal2 + beta*normal3)

## Initialize generator
rnd = TRandom3 (1236)

## Generate one events
def generate_event(fmax, function):

  n = 0
  while True:
    x = rnd.Uniform(xmin, xmax);
    f = function(x)

    # update f if new maximum found
    if f> fmax: fmax = f

    # return the value once accepted  
    y = rnd.Uniform(0, fmax)
    if y < f: return x, fmax

    # update the counter
    n += 1
    if n>1e8:
      raise RuntimeError("Generation not efficient. n > 1e8")

## Set stat box style
gStyle.SetOptStat(1111)

########################
## generate data
########################

## specic choice of true paramters for pseudo data
alfa = 0.5
beta = 0.5

## wrap the multi parameter function to single parameter function
f_truth = lambda x : f_scatter(x, alfa, beta)

## find the coarse maximum of the function
steps = 100
fmax = max(f_truth(xmin+i*(xmax-xmin)/steps) for i in range(steps+1))

## create pseudo data
n = 1000
events = []
for i in range(n):
  x, fmax = generate_event(fmax, f_truth)
  events.append( x )

########################
## analysis and dump info to tree
########################

hdata = TH1D("hdata", "hdata; cos(#theta); Events", 40, -1, 1)
fout = TFile.Open("scatter.root", "recreate")
tree = TTree("scatter", "Sample of scattering")

## create a branch in PyROOT
x = array('d', [0])
tree.Branch("x", x, "x/D")

## fill with pseudo-data
for event in events:
  hdata.Fill( event )

  ## to save into tree -> tree will be used with RooFit in the next examples
  x[0] = event
  tree.Fill()

## Save the output file
fout.Write()
fout.Close()

cf = TCanvas("cf", "cf", 0, 0, 800, 600)
hdata.Draw()
cf.Print("MLE_example03.c1.pdf")

########################
## Maximum likelihood analysis
########################
## two parameter (xx[0], xx[1]) likelihood function which takes data (events) as input
def logLike (xx):

  alfa = xx[0]
  beta = xx[1]

  logL = sum(math.log(f_scatter(x, alfa, beta)) for x in events)

  return -logL

## Now set the infrastructure to minimize maximize LogL (or minimize -logL)
## adapted from: http://root.cern.ch/drupal/content/numerical-minimization
##
## Choose method upon creation between:
## kMigrad, kSimplex, kCombined, 
## kScan, kFumili
minimizer = Minuit2.Minuit2Minimizer ( Minuit2.kMigrad )
minimizer.SetMaxFunctionCalls(1000000)
minimizer.SetMaxIterations(100000)
minimizer.SetTolerance(0.001)

# wrapper of a function to Functor on which Minuit2Minimizer operaters
fLogLike = Math.Functor(logLike, 2)

# initial values of parameters and step size
step = array('d', [0.01,0.01])
variable = array('d', [0.8,0.8])

## assign function to minimizer
minimizer.SetFunction(fLogLike)

## Set the free variables to be minimized!
minimizer.SetVariable(0,"x",variable[0], step[0])
minimizer.SetVariable(1,"y",variable[1], step[1])

## run!
minimizer.Minimize()

## results
xs = minimizer.X()
print("Minimum: f( alfa = ", xs[0], ", beta = ", xs[1], "): ", logLike(xs))

cov = array('d', [0]*4)
minimizer.GetCovMatrix(cov)

print("Covariance matrix:")
print("{:10f} {:10f}".format(cov[0], cov[1]))
print("{:10f} {:10f}".format(cov[2], cov[3]))


print("Uncertainties:")
print(" sigma_alfa = ", math.sqrt(cov[0])) 
print(" sigma_beta = ", math.sqrt(cov[3]))  
print("Correlation rho =  ", cov[1] / math.sqrt(cov[0]) / math.sqrt(cov[3]));

########################
## now investigate experiments to visualise correlation
## For nexp similar experiments maximize likelihood
## Notice how estimate of the the statistical uncertainty from single experiment above compares
## to statistica fluctuation from pseudoexperiments
########################

print("Calculating pseudoexperiments ... ")
nexp = 500

h_alfa_beta = TH2D("h_alfa_beta", "h_alfa_beta;#hat{#alpha};#hat{#beta}", 40, 0, 1, 40, 0, 1)
h_alfa = TH1D("h_alfa", "h_alfa;#hat{#alpha};events", 40, 0, 1)
h_beta = TH1D("h_beta", "h_beta;#hat{#beta};events", 40, 0, 1)
h_logLmax = TH1D("h_logLmax", "Minimum of -logLikelihood;Min(-logLikelihood);events", 5000, 550, 650)

for ie in range(nexp):
  events.clear()
  for i in range(n):
    x, fmax = generate_event(fmax, f_truth)
    events.append( x )

  variable = array('d', [0.8,0.8])

  ## Reset the free variables to be minimized
  minimizer.SetVariable(0,"x",variable[0], step[0])
  minimizer.SetVariable(1,"y",variable[1], step[1])
  minimizer.Minimize()
  xs = minimizer.X()
  h_alfa_beta.Fill(xs[0], xs[1])
  h_alfa.Fill(xs[0])
  h_beta.Fill(xs[1])
  h_logLmax.Fill( logLike(xs))

print("Unceratinties from results of pseudoexperiments:")
print(" sigma_alfa = " , h_alfa.GetRMS())
print(" sigma_beta = " , h_beta.GetRMS())
print("Correlation rho =  ", h_alfa_beta.GetCorrelationFactor())

c2 = TCanvas("c2", "c2", 0, 0, 800, 600)
c2.Divide(2,2)

c2.cd(1)
h_alfa_beta.Draw()
c2.cd(2)
h_alfa.Draw()
c2.cd(3)
h_beta.Draw()

c2.cd(4)
h_logLmax.Draw()

c2.Print("MLE_example03.c2.pdf")




