"""
Example 6: 2-dimensional fit and the likelihood contours
"""

from ROOT import RooDataSet, RooRealVar, RooPolynomial, RooPlot, RooMinimizer, RooMinuit, TFile, TTree, TCanvas, TGraph, TMath, TH2D, RooArgSet, RooFit
from ROOT import kBlue, kYellow, kWhite, kBlack
import math
from array import array

## define observable: RooFit class RooRealVar
x = RooRealVar("x", "x", 0, -0.95, 0.95) ## value, range_min, range_max (N.B. the value doesn't matter here)

## Load the TTree form the input file
## NOTE: it is assumed that the "scatter.root" file is already created 
##       by running lesson3_example03.cpp
inFile = TFile.Open("scatter.root")
t = inFile.Get("scatter")

## convert data to the RooFit class RooDataSet. The imported tree must have a branch named "x"
data = RooDataSet("data", "data", t, RooArgSet(x))

############################/
## define fit model:
############################/

## Parameters: again RooRealVar classes
alpha = RooRealVar("alpha", "#alpha", 0.8, 0, 1) ## initial value, range_min, range_max
beta  = RooRealVar("beta",  "#beta",  0.8, 0, 1) ## initial value, range_min, range_max

## model: using RooPolynomial class
model = RooPolynomial("model", "aaa", x, RooArgSet(alpha, beta))

## perform the fit:
model.fitTo(data, RooFit.Save(True))

## plot the result: using RooFit class RooPlot
c1 = TCanvas("c1", "ML fit", 200, 200, 800, 600)

frame_final = x.frame()
data .plotOn(frame_final, RooFit.Binning(50), RooFit.LineColor(kBlack), RooFit.LineWidth(2), RooFit.MarkerSize(1.2), RooFit.XErrorSize(1.))
model.plotOn(frame_final, RooFit.LineColor(kBlue), RooFit.LineWidth(3))

frame_final.Draw()

############################/
## Make negative log likelihood contour plot
############################/
## Construct unbinned likelihood of model w.r.t. data
nll = model.createNLL(data)

## Create MINUIT interface object
m = RooMinuit(nll)

## Make contour plot of alpha vs beta
c2 = TCanvas("c2", "logL contour plot", 200, 200, 800, 600)
frame_contour = m.contour(alpha,beta, math.sqrt(2.3), math.sqrt(6.18))
frame_contour.SetTitle("RooMinuit contour plot")
frame_contour.Draw()

## draw True value
g = TGraph(1)
g.SetPoint(0, 0.5, 0.5)
g.SetMarkerStyle(2)
g.Draw("p")

############################/
## Generate pseudo-experiments with True alpha and beta
############################/
## histogram to hold pseudo-results
h_pseudo = TH2D("h_pseudo", "results of fits to the pseudo-experiments", 100, -2, 2, 100, -2, 2)

nexp = 10000
nEvt = t.GetEntries() ## size of the sample 
for i in range(nexp):
  ## reset parameters to the known True value
  alpha.setVal(0.5)
  beta .setVal(0.5)
  
  ## generate pseudo-data using RooFit:
  pseudo_data = model.generate(x, nEvt)
  
  ## perform fit on pseudo-data
  model.fitTo(pseudo_data)
  
  ## fill the histogram
  h_pseudo.Fill(alpha.getVal(), beta.getVal())


## draw the histogram
h_pseudo.SetLineColor(kBlack)
contours = array('d', [ h_pseudo.GetMaximum() * math.exp(-0.5*2.3) ])
h_pseudo.SetContour(1, contours)

h_pseudo.Draw("same cont1")
  

