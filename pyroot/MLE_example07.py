"""
Example 2: binned maximum likelihood

Fit histogram h with exponential using
     a) least square fit assuming all bin errors are 1
     b) least square fit assuming all bin errors are sqrt(n_i)
     c) binned maximum likelihood with poisson PDF
  - compare the result with unbinned ML fit from example 2

"""


from ROOT import RooDataSet, RooRealVar, RooExponential, RooPlot, TFile, TTree, TCanvas, TGraph, TMath, TH2D, TF1, TString, TLegend, RooArgList, TH1D
from ROOT import RooFit, kBlue, kYellow, kWhite, kBlack, kRed, kMagenta, kCyan, kBlue


####################################################################/
## model
####################################################################/

t = RooRealVar("t", "t", 0, 0, 5)  ## observable
Gamma = RooRealVar("Gamma", "Gamma", -1.5, -10., 0.)  ## parameter

model = RooExponential("model", "Exp(t*Gamma)", t, Gamma)

####################################################################/
## generate data
####################################################################/
nEvt = 50
data = model.generate(t, nEvt)

####################################################################/
## fit the model
####################################################################/
model.fitTo(data)

####################################################################/
## display data
####################################################################/
## plot the result: using RooFit class RooPlot
c1 = TCanvas("c1", "Fit", 200, 200, 800, 600)

frame_final = t.frame()
data .plotOn(frame_final, RooFit.Binning(10), RooFit.LineColor(kBlack), RooFit.LineWidth(2), RooFit.MarkerSize(1.2), RooFit.XErrorSize(1.))
model.plotOn(frame_final, RooFit.LineColor(kBlue), RooFit.LineWidth(3))

frame_final.Draw()

####################################################################/
## binned fits in ROOT
####################################################################/
## convert RooDataSet to a ROOT histogram
hdata = TH1D("hdata", "hdata; t; events", 10, 0, 5)
data.fillHistogram(hdata, RooArgList(t))

####################################################################/
## DEFINE FITTED FUNCTIONS
####################################################################/
functions = []
for i in range(4):
  fun = TF1("fun{}".format(i), "-[1]*[0]*TMath::Exp([0]*x)", 0, 5)
  ## set the starting point
  fun.SetParameter(0, -1.5) ## mean lifetime. We "don't know" the value so just guess
  fun.FixParameter(1, nEvt*5.0/hdata.GetNbinsX())   ## normalization factor. Will be fixed
  functions.append( fun )


####################################################################/
## PERFORM FITS
####################################################################/
hdata.Fit(functions[0], "W I 0")
hdata.Fit(functions[1], "0 I" )
hdata.Fit(functions[2], "L I 0")

## draw functions:
functions[0].SetLineColor(kRed)
functions[1].SetLineColor(kMagenta)
functions[2].SetLineColor(kCyan)
functions[3].SetLineColor(kBlue)
functions[3].SetLineWidth(3)

for i in range(3):
  functions[i].Draw("same")


## Draw legend
leg = TLegend(0.5, 0.85, 0.85, 0.4)
leg.SetFillColor(kWhite)
leg.SetTextFont(42)
leg.SetTextSize(0.05)
leg.AddEntry(functions[0], "Chi2 fit all weights 1", "l")
leg.AddEntry(functions[1], "Chi2 fit", "l")
leg.AddEntry(functions[2], "Binned ML fit", "l")
leg.AddEntry(functions[3], "Unbinned ML fit", "l")
leg.Draw("same")

print("Chi2 fit all weights 1: ", functions[0].GetParameter(0), "+/-", functions[0].GetParError(0)); 
print("Chi2 fit              : ", functions[1].GetParameter(0), "+/-", functions[1].GetParError(0)); 
print("Binned ML fit         : ", functions[2].GetParameter(0), "+/-", functions[2].GetParError(0)); 
print("Unbinned ML fit       : ", Gamma.getVal(), "+/-",  Gamma.getError()); 
