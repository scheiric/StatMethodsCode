""" RooFit example
"""

from ROOT import RooMinimizer, RooMinuit, TFile, TTree, TCanvas, TGraph, TH2D, TLine, TLegend, RooRealVar, RooDataSet, RooArgSet, RooPolynomial
from ROOT import RooFit, kBlack, kBlue, kWhite

## define observable: RooFit class RooRealVar
x = RooRealVar("x", "x", 0, -0.95, 0.95) # value, range_min, range_max (N.B. the value doesn't matter here)

## Load the TTree form the input file
## NOTE: it is assumed that the "scatter.root" file is already created 
##       by running lesson3_example03.cpp or lesson3_example03.py
inFile = TFile.Open("scatter.root")
t = inFile.Get("scatter")

## convert data to the RooFit class RooDataSet. The imported tree must have a branch named "x"
data = RooDataSet("data", "data", t, RooArgSet(x))

############################
## define fit model:
############################

## Parameters: again RooRealVar classes
alpha = RooRealVar("alpha", "#alpha", 0.8, -2, 2);  ## initial value, range_min, range_max
beta = RooRealVar("beta",  "#beta",  0.8, -2, 2)   ## initial value, range_min, range_max

## model: using RooPolynomial class
model = RooPolynomial("model", "1+alpha*x + beta*x^2", x, RooArgSet(alpha, beta))

## perform the fit:
model.fitTo(data, RooFit.Save(True))

## plot the result: using RooFit class RooPlot
c1 = TCanvas("c1", "ML fit", 0, 10, 800, 600)

frame_final = x.frame()
data .plotOn(frame_final, RooFit.Binning(50), RooFit.LineColor(kBlack), RooFit.LineWidth(2), RooFit.MarkerSize(1.2), RooFit.XErrorSize(1.))
model.plotOn(frame_final, RooFit.LineColor(kBlue), RooFit.LineWidth(3))

frame_final.Draw()

  

############################
## Make negative log likelihood contour plot
############################
print("Contour plot --------------------------------------------------------------")
## Construct unbinned likelihood of model w.r.t. data
nll = model.createNLL(data)

## Create MINUIT interface object
m = RooMinuit(nll)  

## Run HESSE and to calculate errors from d2L/dp2
m.migrad()
m.hesse()

##  Make contour plot of alpha vs beta at 1,2,3 sigma
c2 = TCanvas("c2", "logL contour plot", 0, 10, 800, 600)
axs = TH2D("axs", ";#alpha;#beta", 100, 0.3, 0.9, 100, -0.5, 1.3 )
axs.SetStats(0)
axs.SetLineColor(kBlue)
axs.SetLineWidth(2)
axs.SetMarkerStyle(20)
axs.Draw()
frame_contour = m.contour(alpha,beta)
frame_contour.SetTitle("RooMinuit contour plot")
frame_contour.Draw("same")

## draw true value
g = TGraph(1)
g.SetPoint(0, 0.5, 0.5)
g.SetMarkerSize(3)
g.SetLineWidth(2)
g.SetMarkerStyle(2)
g.Draw("p")

############################
## 1D error bars
############################
print()  
print("1D errors:")
print("  alpha = ", alpha.getVal(), " +/- ", alpha.getError())
print("  beta  = ", beta.getVal() , " +/- ", beta.getError())

a  = alpha.getVal();
ae = alpha.getError();
b  = beta.getVal();
be = beta.getError();
errorBox = []
errorBox.append(TLine(a-ae, b-be, a+ae, b-be))
errorBox.append(TLine(a-ae, b+be, a+ae, b+be))
errorBox.append(TLine(a-ae, b-be, a-ae, b+be))
errorBox.append(TLine(a+ae, b-be, a+ae, b+be))
for line in errorBox:
    line.SetLineStyle(5)
    line.Draw()


## draw legend
l = TLegend(0.2, 0.2, 0.89, 0.43)
l.SetTextFont(42)
l.SetTextSize(0.05)
l.AddEntry(g, "Input values", "p")
l.AddEntry(axs, "ML estimate", "p")
l.AddEntry(axs, "Likelihood contour", "l")
l.AddEntry(errorBox[0], "1D error box", "l")
l.SetFillColor(kWhite)
l.Draw("same")
