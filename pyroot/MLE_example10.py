"""
example of the constrained likelihood fit.
We will fit the mean mass and resolution on a control sample and
use it to extract the number of events in the signal sample
"""
from ROOT import RooDataSet, RooRealVar, RooGaussian, RooChebychev, RooPolynomial, RooAddPdf, RooPlot, RooDataSet, TCanvas, TH1D, RooArgSet
from ROOT import RooFit, kBlue, kYellow, kWhite, kBlack, kRed, kMagenta, kCyan, kBlue, kGreen, kDashed


nEvt         = 300  ## size of the data sample
nEvt_control = 2000 ## size of the control sample

m = RooRealVar("m", "m", 0, 100, 150, "GeV") ## mass

##---------------------------------  
## define model for the comtrol samples.
##---------------------------------  
mean_control  = RooRealVar("mean", "#mu", 125, 80, 200, "GeV") ## if no range is given, the value is fixed
sigma_control = RooRealVar("sigma", "#sigma", 2., 0, 10)
fSig_control  = RooRealVar("fSig", "f_{sig}", 0.9, 0, 1)

sigPdf_control = RooGaussian("sigPdf_control", "sigPdf_control", m, mean_control, sigma_control)
bkgPdf_control = RooPolynomial("bkgPdf_control", "bkgPdf_control", m)
model_control = RooAddPdf("model_control", "", RooArgSet(sigPdf_control, bkgPdf_control), RooArgSet(fSig_control))

## generate the data
data_control = model_control.generate(m, nEvt_control)

## perform fit on control sample to extract mean mass and resolution:
model_control.fitTo(data_control)

## display  
c1 = TCanvas("c1", "Control sample", 200, 200, 800, 600)
frame1 = m.frame()

## draw data and PDFs onto the frame1
data_control .plotOn(frame1, RooFit.Binning(30))
model_control.plotOn(frame1, RooFit.LineColor(kBlue), RooFit.LineWidth(3))
frame1.Draw()
  
##---------------------------------  
## define model for the signal sample.
##---------------------------------  
## The signal sample will have different signal fraction and background,
## but it has the same mass and mass resolution:


##---------------------------------  
## define model parameters. Again, class RooRealVar is used.
##---------------------------------  
mean  = RooRealVar("mean", "#mu", 125, 80, 200, "GeV") ## if no range is given, the value is fixed
sigma = RooRealVar("sigma", "#sigma", 2., 0, 10)
slope = RooRealVar("slope", "a_{1}", -0.8, -1, 1)
ns    = RooRealVar("ns", "ns", 0.2*nEvt, 0, 1000000)
nb    = RooRealVar("nb", "nb", (1-0.2)*nEvt, 0, 1000000)

sigPdf = RooGaussian("sigPdf", "sigPdf", m, mean, sigma)
bkgPdf = RooChebychev("bkgPdf", "bkgPdf", m, RooArgSet(slope))
model = RooAddPdf("model", "gsigPdf + bkgPdf", RooArgSet(sigPdf, bkgPdf), RooArgSet(ns,nb))

## generate the signal sample data
data = model.generate(m, nEvt)

## now we create gaussian constraints for mean mass and sigma.
## We have plugged in the best values and uncertainties from the control sample fit:
mean_bestValue = RooRealVar("mean_bestValue", "mean_bestValue", mean_control.getVal())
mean_error = RooRealVar("mean_error", "mean_error", mean_control.getError())

sigma_bestValue = RooRealVar("sigma_bestValue", "sigma_bestValue", sigma_control.getVal())
sigma_error = RooRealVar("sigma_error", "sigma_error", sigma_control.getError())

## Then we construct a constraining PDF using the RooGaussian class:
## NOTE that paramerer "mean" and "sigma" are treated as observables!
constrMeanPdf  = RooGaussian("constrMeanPdf","constrMeanPdf",mean, mean_bestValue, mean_error)
constrSigmaPdf = RooGaussian("constrSigmaPdf","constrSigmaPdf",sigma, sigma_bestValue, sigma_error)

##---------------------------------   
## perform fit
##---------------------------------  
## model.fitTo(*data, Extended(true))  
model.fitTo(data, RooFit.Extended(True), RooFit.ExternalConstraints(RooArgSet(constrMeanPdf, constrSigmaPdf)))  


c2 = TCanvas("c2", "Signal sample", 200, 200, 800, 600)
frame2 = m.frame()

## draw data and PDFs onto the frame1
data .plotOn(frame2, RooFit.Binning(30))
model.plotOn(frame2, RooFit.LineColor(kBlue), RooFit.LineWidth(3))
frame2.Draw()
    
print("ns    = " , ns.getVal() , " +/- " , ns.getError())
print("sigma = " , sigma.getVal() , " +/- " , sigma.getError())

